# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0005_auto_20141020_1623'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Download',
        ),
        migrations.DeleteModel(
            name='Upload',
        ),
        migrations.CreateModel(
            name='Download',
            fields=[
            ],
            options={
                'unique_together': set([('mac', 'vlan', 'ip', 'ip_proto', 'stamp_inserted')]),
                'db_table': 'download',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Upload',
            fields=[
            ],
            options={
                'unique_together': set([('mac', 'vlan', 'ip', 'ip_proto', 'stamp_inserted')]),
                'db_table': 'upload',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
