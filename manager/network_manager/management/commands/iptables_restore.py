# ⁻*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.utils import timezone
import scapy.layers.l2
import scapy.route

from network_manager.models import PortRedirection, MachineFirewall
from network_manager.forms import format_mac
from network_manager.utils import net_to_iface
import network_manager.config as config
import network_manager.utils as utils
class Command(BaseCommand):
    args = ''
    help = u'Régénère les règles iptables dynamiques'

    def add_arguments(self, parser):
        parser.add_argument('--nono', action="store_true")

    def handle(self, *args, **options):
        #for redirect in PortRedirection.objects.all():
        #    if redirect.public_port and redirect.private_port:
        #        self.stdout.write(redirect.command(chain))
        #for po in MachineFirewall.objects.order_by("priority", "-machine").all():
        #    self.stdout.write(po.command(chain))
        utils.iptables_restore(nono=options["nono"])
