# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0002_auto_20141013_1501'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='owner',
            options={'verbose_name': 'Propri\xe9taire'},
        ),
        migrations.AlterField(
            model_name='machine',
            name='owner',
            field=models.ForeignKey(related_name=b'machines', blank=True, to='network_manager.Owner', help_text=b'Si d\xc3\xa9finie, fixe la limite de validit\xc3\xa9 de la machine a celle du propr\xc3\xa9taire', null=True, verbose_name=b'propri\xc3\xa9taire'),
        ),
        migrations.AlterField(
            model_name='owner',
            name='address',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
