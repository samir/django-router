# ⁻*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.utils import timezone

from optparse import make_option

#import scapy.layers.l2
#import scapy.route
#from scapy.all import MTU
import os
import pcap
import socket
import struct
import binascii
import collections
import time
import netaddr

from network_manager.models import Machine, Vlan, MultiMachine, MacAddressWifi
from network_manager.utils import format_mac
import network_manager.config as config

pidfile = '/var/run/arp_daemon.pid'

#rawSocket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0003))
#rawSocket.bind(("eth0.4", 0))
class Command(BaseCommand):
    args = ''
    help = u'Met à jour la date de dernière connexion des machines fixes'
    last_update = collections.defaultdict(int)
    update_interval = config.arp_scan_update_interval # 10 min
    last_clean_up = time.time()
    clean_up_interval = config.arp_scan_clean_up_interval # 1h

    def add_arguments(self, parser):
        parser.add_argument('--debug',
            action='store_true',
            dest='debug',
            default=False,
            help='Debug')

    def add_arguments(self, parser):
        # Positional arguments
        #parser.add_argument('poll_id', nargs='+', type=int)

        # Named (optional) arguments
        parser.add_argument('--debug',
            action='store_true',
            dest='debug',
            default=False,
            help='Debug')

    def handle(self, *args, **options):
        debug = options.get('debug', False)
        with open(pidfile, 'w') as f:
            f.write(str(os.getpid()))
        pc = pcap.pcap(name="eth0", snaplen=58, promisc=False)
        # on filtre ARP et NDP (si pas d'options dans l'entête ipv6, c'est le cas normalement pour NDP)
        pc.setfilter('arp or ip6[40]=135 or ip6[40]=136')
        for ts, packet in pc:
            ethernet_header = packet[0:14]
            ethernet_detailed = struct.unpack("!6s6s2s", ethernet_header)
            ethertype = ethernet_detailed[2]

            #802.1Q
            if ethertype == '\x81\x00':
                vlan_header = packet[14:18]
                vlan_detailed = struct.unpack("!1h2s", vlan_header)
                ethertype = vlan_detailed[1]
                ethernet_detailed = ethernet_detailed[0:2] + (ethertype,)
                vlan = vlan_detailed[0]
                # removing vlan info
                packet = packet[0:12] + packet[16:]
            else:
                vlan = 1
                continue
            # un packet NDP
            if ethertype == '\x86\xdd':
                ipv6=True
                arp=False
            # un packet ARP
            elif ethertype == '\x08\x06':
                ipv6=False
                arp=True
            else:
                continue

            mac_address_raw = ethernet_detailed[1]
            if mac_address_raw in ["\0\0\0\0\0\0", "\xff\xff\xff\xff\xff\xff"]:
                continue
            now = time.time()
            if now - self.last_update[(mac_address_raw, vlan)] > self.update_interval:
                mac_address = format_mac(binascii.hexlify(mac_address_raw))
                machines = Machine.objects.filter(Q(mac_address=mac_address) & Q(vlan=vlan))
                if not machines:
                    try:
                        multi_machine = MultiMachine.objects.get(
                            Q(vlan=vlan) & Q(active=True) &
                            (Q(valid_until__gte=timezone.now().date()) | Q(valid_until=None)) &
                            (Q(valid_from__lte=timezone.now().date()) | Q(valid_from=None))
                        )
                    except MultiMachine.DoesNotExist:
                        multi_machine = None
                    is_mac_wifi = len(MacAddressWifi.objects.filter(mac_address=mac_address)) > 0

                if machines:
                    machines.filter(Q(vlan=vlan)).update(last_connection=timezone.now())
                    self.last_update[(mac_address_raw, vlan)] = now
                elif multi_machine is not None and (multi_machine.has_mac(mac_address) or (not is_mac_wifi and multi_machine.fixe)):
                    if not is_mac_wifi and multi_machine.fixe:
                        multi_machine.add_mac_address_fixe(mac_address)
                    multi_machine.last_connection=timezone.now()
                    multi_machine.save()
                    self.last_update[(mac_address_raw, vlan)] = now
                # On ne crée de nouvelle machine que pour les machines dont on est sûr
                # quelles ont un adressage ipv4, et s'il n'y a pas de multi machine activé.
                # Sinon, on risque de créer des machines wifi se connectant en multimachine
                elif arp:
                    arp_header = packet[14:42]
                    arp_detailed = struct.unpack("2s2s1s1s2s6s4s6s4s", arp_header)
                    ipv4 = socket.inet_ntoa(arp_detailed[6])
                    try:
                        vlan = Vlan.objects.get(id=vlan)
                        if (
                            vlan.auto_register and vlan.network_ipv4 and
                            netaddr.IPAddress(ipv4) in netaddr.IPNetwork(vlan.network_ipv4) and
                            not is_mac_wifi
                        ):
                            Machine.new(mac_address, ipv4, vlan=vlan)
                    except Vlan.DoesNotExist:
                        pass

            if now - self.last_clean_up > self.clean_up_interval:
                for mac, vlan in self.last_update.keys():
                   if now - self.last_update[(mac, vlan)] > self.update_interval:
                       del(self.last_update[(mac, vlan)])
                self.last_clean_up = now
                    

            if debug:
                print "****************_ETHERNET_FRAME_****************"
                print "Dest MAC:        ", format_mac(binascii.hexlify(ethernet_detailed[0]))
                print "Source MAC:      ", format_mac(binascii.hexlify(ethernet_detailed[1]))
                print "Type:            ", binascii.hexlify(ethertype)
                print "VLAN:            ", vlan
                if arp:
                    arp_header = packet[14:42]
                    arp_detailed = struct.unpack("2s2s1s1s2s6s4s6s4s", arp_header)
                    print "Source IP:       ", socket.inet_ntoa(arp_detailed[6])
                    print "Dest IP:         ", socket.inet_ntoa(arp_detailed[8])
                if ipv6:
                    ipv6_header = packet[14:54]
                    ipv6_detailed = struct.unpack("4s4s16s16s", ipv6_header)
                    print "Source IP:       ", socket.inet_ntop(socket.AF_INET6, ipv6_detailed[2])
                    print "Dest IP:         ", socket.inet_ntop(socket.AF_INET6, ipv6_detailed[3])
