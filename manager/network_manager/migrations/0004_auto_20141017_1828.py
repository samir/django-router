# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0003_auto_20141013_1521'),
    ]

    operations = [
        migrations.AlterField(
            model_name='machine',
            name='owner',
            field=models.ForeignKey(related_name=b'machines', blank=True, to='network_manager.Owner', help_text=b'Si d\xc3\xa9finie, fixe la limite de validit\xc3\xa9 de la machine \xc3\xa0 celle du propr\xc3\xa9taire', null=True, verbose_name=b'propri\xc3\xa9taire'),
        ),
        migrations.AlterField(
            model_name='owner',
            name='address',
            field=models.CharField(max_length=255, null=True, verbose_name=b'adresse', blank=True),
        ),
        migrations.AlterField(
            model_name='owner',
            name='name',
            field=models.CharField(unique=True, max_length=255, verbose_name=b'nom'),
        ),
        migrations.AlterField(
            model_name='owner',
            name='valid_until',
            field=models.DateField(help_text=b"Date jusqu'\xc3\xa0 laquelle les machines de l'utilisateur peuvent s'authentifier. Laissez vide pour pas de limite ou pour mettre des limites par machine.", null=True, verbose_name=b'limite de validit\xc3\xa9', blank=True),
        ),
    ]
