from django.shortcuts import render

import sys

# Create your views here.
def boot(request):
    mac = request.GET.get('mac', "")
    if mac == "78:24:af:3c:aa:0e":
        return render(request, "pxe/airelle", {}, content_type="text/plain")
    else:
        return render(request, "pxe/boot", {'mac': mac}, content_type="text/plain")
