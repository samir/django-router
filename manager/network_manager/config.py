# ⁻*- coding: utf-8 -*-
import netaddr

dhcp_host_template = u"""
host %(host)s {
    hardware ethernet %(mac)s;
    fixed-address %(ipv4)s;
    option host-name "%(hostname)s";
    ddns-hostname "%(hostname)s";
    default-lease-time 86400;
    max-lease-time 86400;
}
"""

ddns_server = '127.0.0.1'
ddns_domain = "cmg.genua.fr"
ddns_key_path = "/etc/bind/ddns-keys/ddns.key"
ddns_ttl = 600

allocate_public_ip = True

arp_scan_update_interval = 600
arp_scan_clean_up_interval = 3600

private_ips_exception = ["212.27.38.253"]
private_ips_exception = [netaddr.IPAddress(ip) for ip in private_ips_exception]

iptables_default_chains = ['INPUT', 'OUTPUT', 'FORWARD', 'PREROUTING', 'POSTROUTING']
ipables_tables = ['raw', 'mangle', 'filter', 'nat']

ipt_mark = {
  'masquerade' : 1,
  'mac_ip' : 2,
  'mac_ip_multi' : 3,
}
