# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Interface'
        db.create_table('network_manager_interface', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('uplink', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('network_manager', ['Interface'])

        # Adding model 'Vlan'
        db.create_table('network_manager_vlan', (
            ('id', self.gf('django.db.models.fields.IntegerField')(unique=True, primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('network_ipv4', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('network_ipv6', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('interface', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, related_name='vlan', unique=True, null=True, to=orm['network_manager.Interface'])),
        ))
        db.send_create_signal('network_manager', ['Vlan'])

        # Adding model 'Ipv4Address'
        db.create_table('network_manager_ipv4address', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ip', self.gf('django.db.models.fields.IPAddressField')(unique=True, max_length=15)),
            ('bin', self.gf('django.db.models.fields.BigIntegerField')(unique=True, null=True, blank=True)),
            ('public', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('vlan', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['network_manager.Vlan'])),
        ))
        db.send_create_signal('network_manager', ['Ipv4Address'])

        # Adding model 'Machine'
        db.create_table('network_manager_machine', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('wifi', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('last_connection', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('valid_until', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('mac_address', self.gf('django.db.models.fields.CharField')(max_length=18, null=True, blank=True)),
            ('wifi_password', self.gf('django.db.models.fields.CharField')(default=u'epkpr7837g3tki3', unique=True, max_length=255)),
            ('manufacturer', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('ipv4_address', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, related_name='machine', unique=True, null=True, to=orm['network_manager.Ipv4Address'])),
            ('has_public_ipv4_address', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('vlan', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['network_manager.Vlan'])),
        ))
        db.send_create_signal('network_manager', ['Machine'])

        # Adding unique constraint on 'Machine', fields ['name', 'wifi']
        db.create_unique('network_manager_machine', ['name', 'wifi'])

        # Adding unique constraint on 'Machine', fields ['mac_address', 'vlan']
        db.create_unique('network_manager_machine', ['mac_address', 'vlan_id'])

        # Adding model 'MachineFirewall'
        db.create_table('network_manager_machinefirewall', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('priority', self.gf('django.db.models.fields.IntegerField')(default=100)),
            ('machine', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='firewall_rules', null=True, to=orm['network_manager.Machine'])),
            ('vlan', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='firewall_rules', null=True, to=orm['network_manager.Vlan'])),
            ('incoming', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('ip_protocol', self.gf('django.db.models.fields.CharField')(default='ipv6', max_length=4)),
            ('allow', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('protocol', self.gf('django.db.models.fields.CharField')(max_length=3, null=True, blank=True)),
            ('ports', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal('network_manager', ['MachineFirewall'])

        # Adding unique constraint on 'MachineFirewall', fields ['machine', 'incoming', 'protocol', 'allow', 'vlan', 'ip_protocol']
        db.create_unique('network_manager_machinefirewall', ['machine_id', 'incoming', 'protocol', 'allow', 'vlan_id', 'ip_protocol'])

        # Adding model 'PortRedirection'
        db.create_table('network_manager_portredirection', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('public_machine', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['network_manager.Machine'])),
            ('private_machine', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['network_manager.Machine'])),
            ('public_port', self.gf('network_manager.fields.IntegerRangeField')(null=True, blank=True)),
            ('private_port', self.gf('network_manager.fields.IntegerRangeField')(null=True, blank=True)),
            ('protocol', self.gf('django.db.models.fields.CharField')(max_length=3)),
        ))
        db.send_create_signal('network_manager', ['PortRedirection'])

        # Adding unique constraint on 'PortRedirection', fields ['public_machine', 'public_port', 'protocol']
        db.create_unique('network_manager_portredirection', ['public_machine_id', 'public_port', 'protocol'])


    def backwards(self, orm):
        # Removing unique constraint on 'PortRedirection', fields ['public_machine', 'public_port', 'protocol']
        db.delete_unique('network_manager_portredirection', ['public_machine_id', 'public_port', 'protocol'])

        # Removing unique constraint on 'MachineFirewall', fields ['machine', 'incoming', 'protocol', 'allow', 'vlan', 'ip_protocol']
        db.delete_unique('network_manager_machinefirewall', ['machine_id', 'incoming', 'protocol', 'allow', 'vlan_id', 'ip_protocol'])

        # Removing unique constraint on 'Machine', fields ['mac_address', 'vlan']
        db.delete_unique('network_manager_machine', ['mac_address', 'vlan_id'])

        # Removing unique constraint on 'Machine', fields ['name', 'wifi']
        db.delete_unique('network_manager_machine', ['name', 'wifi'])

        # Deleting model 'Interface'
        db.delete_table('network_manager_interface')

        # Deleting model 'Vlan'
        db.delete_table('network_manager_vlan')

        # Deleting model 'Ipv4Address'
        db.delete_table('network_manager_ipv4address')

        # Deleting model 'Machine'
        db.delete_table('network_manager_machine')

        # Deleting model 'MachineFirewall'
        db.delete_table('network_manager_machinefirewall')

        # Deleting model 'PortRedirection'
        db.delete_table('network_manager_portredirection')


    models = {
        'network_manager.interface': {
            'Meta': {'object_name': 'Interface'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'uplink': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'network_manager.ipv4address': {
            'Meta': {'ordering': "('bin',)", 'object_name': 'Ipv4Address'},
            'bin': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'unique': 'True', 'max_length': '15'}),
            'public': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'vlan': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['network_manager.Vlan']"})
        },
        'network_manager.machine': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('name', 'wifi'), ('mac_address', 'vlan'))", 'object_name': 'Machine'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'has_public_ipv4_address': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ipv4_address': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'machine'", 'unique': 'True', 'null': 'True', 'to': "orm['network_manager.Ipv4Address']"}),
            'last_connection': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'mac_address': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'}),
            'manufacturer': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'valid_until': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'vlan': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['network_manager.Vlan']"}),
            'wifi': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'wifi_password': ('django.db.models.fields.CharField', [], {'default': "u'3nbeqyguvymef37'", 'unique': 'True', 'max_length': '255'})
        },
        'network_manager.machinefirewall': {
            'Meta': {'ordering': "('incoming', 'priority', '-machine')", 'unique_together': "(('machine', 'incoming', 'protocol', 'allow', 'vlan', 'ip_protocol'),)", 'object_name': 'MachineFirewall'},
            'allow': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incoming': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ip_protocol': ('django.db.models.fields.CharField', [], {'default': "'ipv6'", 'max_length': '4'}),
            'machine': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'firewall_rules'", 'null': 'True', 'to': "orm['network_manager.Machine']"}),
            'ports': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'protocol': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'vlan': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'firewall_rules'", 'null': 'True', 'to': "orm['network_manager.Vlan']"})
        },
        'network_manager.portredirection': {
            'Meta': {'unique_together': "(('public_machine', 'public_port', 'protocol'),)", 'object_name': 'PortRedirection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'private_machine': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['network_manager.Machine']"}),
            'private_port': ('network_manager.fields.IntegerRangeField', [], {'null': 'True', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'public_machine': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['network_manager.Machine']"}),
            'public_port': ('network_manager.fields.IntegerRangeField', [], {'null': 'True', 'blank': 'True'})
        },
        'network_manager.vlan': {
            'Meta': {'ordering': "('id',)", 'object_name': 'Vlan'},
            'id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'interface': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'vlan'", 'unique': 'True', 'null': 'True', 'to': "orm['network_manager.Interface']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'network_ipv4': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'network_ipv6': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['network_manager']