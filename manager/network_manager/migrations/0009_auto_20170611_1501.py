# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0008_auto_20170611_1229'),
    ]

    operations = [
        migrations.CreateModel(
            name='MacAddressFixe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mac_address', models.CharField(max_length=18, null=True, verbose_name=b'Adresse mac', blank=True)),
                ('machine', models.ForeignKey(to='network_manager.MultiMachine')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MacAddressWifi',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mac_address', models.CharField(max_length=18, null=True, verbose_name=b'Adresse mac', blank=True)),
                ('machine', models.ForeignKey(to='network_manager.MultiMachine')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='macaddress',
            unique_together=None,
        ),
        migrations.RemoveField(
            model_name='macaddress',
            name='machine',
        ),
        migrations.DeleteModel(
            name='MacAddress',
        ),
        migrations.AlterUniqueTogether(
            name='macaddresswifi',
            unique_together=set([('mac_address', 'machine')]),
        ),
        migrations.AlterUniqueTogether(
            name='macaddressfixe',
            unique_together=set([('mac_address', 'machine')]),
        ),
    ]
