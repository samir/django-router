# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0009_auto_20170611_1501'),
    ]

    operations = [
        migrations.AlterField(
            model_name='machinefirewall',
            name='protocol',
            field=models.CharField(blank=True, max_length=4, null=True, verbose_name=b'protocole', choices=[(b'udp', b'udp'), (b'tcp', b'tcp'), (b'icmp', b'icmp')]),
            preserve_default=True,
        ),
    ]
