import time
from django.conf import settings
from django.contrib import auth


class AutoLogout(object):
  def process_request(self, request):
    if request.user.is_authenticated():
      try:
        if int(time.time()) - request.session['last_touch'] > settings.AUTO_LOGOUT_DELAY:
          auth.logout(request)
          del request.session['last_touch']
          return
      except KeyError:
        pass

      request.session['last_touch'] = int(time.time())
