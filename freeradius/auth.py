## ⁻*- coding: utf-8 -*-
import eventlet
eventlet.monkey_patch(thread=True)

import sys
import traceback
import os
import time
import datetime

sys.path.insert(0, "/usr/scripts/manager/")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "manager.settings")
import django

django.setup()

from django.db.models import Q
from django.utils import timezone
from django.db import connection, OperationalError

try:
    import radiusd
except ImportError:
    import radiusd_debug as radiusd
import netaddr

from network_manager.models import Machine, MultiMachine

CACHE = {}
CACHE_TIMEOUT = 60
debug = False

def format_mac(mac):
    """ Formatage des adresses mac
    Transforme une adresse pour obtenir la forme xx:xx:xx:xx:xx:xx
    Retourne la mac formatée.
    """
    if '-' in mac:
        mac = mac.split(':')[0]
    mac = unicode(mac).lower()
    if mac == u'<automatique>':
        return mac
    mac = netaddr.EUI(mac)
    if not mac:
        raise ValueError(u"MAC nulle interdite\nIl doit être possible de modifier l'adresse de la carte.")
    return unicode(str(mac).replace('-', ':')).lower()


def log_exception(e=None):
    rlog('Exception: %r' % e, typ=radiusd.L_ERR)
    for line in traceback.format_exc().splitlines():
        rlog(line, typ=radiusd.L_ERR)

def radius_event(f):
    global CACHE
    """Décorateur pour les fonctions d'interfaces avec radius.
    Une telle fonction prend un uniquement argument, qui est une liste de tuples
    (clé, valeur) et renvoie un triplet dont les composantes sont :
     * le code de retour (voir radiusd.RLM_MODULE_* )
     * un tuple de couples (clé, valeur) pour les valeurs de réponse (accès ok
       et autres trucs du genre)
     * un tuple de couples (clé, valeur) pour les valeurs internes à mettre à
       jour (mot de passe par exemple)
    Voir des exemples plus complets ici:
    https://github.com/FreeRADIUS/freeradius-server/blob/master/src/modules/rlm_python/

    On se contente avec ce décorateur (pour l'instant) de convertir la liste de
    tuples en entrée en un dictionnaire."""

    def new_f(auth_data):
        data = dict()
        for (key, value) in auth_data or []:
            # Beware: les valeurs scalaires sont entre guillemets
            # Ex: Calling-Station-Id: "une_adresse_mac"
            data[key] = value.replace('"', '')
        try:
            return f(data)
        except OperationalError:
            CACHE = {}
            connection.connection.close()
            connection.connection = None
            return f(data)
        except Exception as e:
            CACHE = {}
            log_exception(e)
        except:
            CACHE = {}
            log_exception()
            #logger.error(repr(e) + ' on data ' + repr(auth_data))
        return radiusd.RLM_MODULE_REJECT

    return new_f



@radius_event
def instantiate(p, *conns):
    """Utile pour initialiser les connexions ldap une première fois (otherwise,
    do nothing)"""
    rlog('instanciate', ldebug=True)
    pass

def get_from_cache(key, now=None, expire=CACHE_TIMEOUT):
    global CACHE
    if now is None:
        now = time.time()
    if key in CACHE:
        (ts, v) = CACHE[key]
        if now - ts < expire:
            return v
        else:
            raise ValueError("Expired")
    else:
        raise ValueError("Not Found")

def set_to_cache(keys, value, now=None, null=False):
    global CACHE
    if now is None:
        now = time.time()
    if not isinstance(keys, list):
        keys = [keys]
    for key in keys:
        if key or null:
            CACHE[key] = (now, value)

def get_nas(data):
    mac = data.get('Called-Station-Id', '00:26:5a:ba:bb:3f')
    if mac:
        try:
            mac = format_mac(mac.decode('ascii', 'ignore'))
        except:
            radiusd.radlog(radiusd.L_ERR, '[python] Cannot format AP MAC %r !' % mac)
            mac = None
    if mac is None:
        radiusd.radlog(radiusd.L_ERR, '[python] Cannot read AP MAC !')
        raise ValueError("Cannot read AP MAC")
    now = time.time()
    try:
        return get_from_cache(mac, now, expire=3600)
    except ValueError:
        machine =  Machine.objects.get(
            Q(mac_address=mac) & Q(active=True) & Q(wifi=False) &
            (Q(valid_until__gte=timezone.now().date()) | Q(valid_until=None)) &
            (Q(valid_from__lte=timezone.now().date()) | Q(valid_from=None))
        )
        set_to_cache([mac], machine, now)
        return machine

def get_machine(data):
    """Obtient la machine essayant actuellement de se connecter"""
    global CACHE
    mac = data.get('Calling-Station-Id', None)
    now = time.time()
    if mac:
        try:
            mac = format_mac(mac.decode('ascii', 'ignore'))
        except:
            radiusd.radlog(radiusd.L_ERR, '[python] Cannot format MAC !')
            mac = None
    username = data.get('User-Name', None)
    if username:
        username = username.decode('ascii', 'ignore')

    if mac is None:
        radiusd.radlog(radiusd.L_ERR, '[python] Cannot read client MAC from AP !')
    if username is None:
        radiusd.radlog(radiusd.L_ERR, '[python] Cannot read client User-Name !')

    # Liste de recherches ldap à essayer, dans l'ordre
    # ** Case 1: Search by mac
    if mac:
        try:
            return get_from_cache(mac, now)
        except ValueError:
            try:
                ap = get_nas(data)
                machine = Machine.objects.get(
                    Q(mac_address=mac) & Q(active=True) & Q(wifi=True) & Q(vlan=ap.vlan) &
                    (Q(valid_until__gte=timezone.now().date()) | Q(valid_until=None)) &
                    (Q(valid_from__lte=timezone.now().date()) | Q(valid_from=None))
                )
                set_to_cache([mac, username], machine, now)
                return machine
            except (Machine.DoesNotExist, ValueError):
                pass

    # ** Case 2: unregistered mac : il nous faut au moins un username ou être sûr
    # du propriétaire
    if username:
        try:
            return get_from_cache(username, now)
        except ValueError:
            try:
                ap = get_nas(data)
                machine = Machine.objects.get(
                    Q(name=username) & Q(active=True) & Q(wifi=True) & Q(vlan=ap.vlan) &
                    (Q(mac_address=None) | Q(mac_address="")) &
                    (Q(valid_until__gte=timezone.now().date()) | Q(valid_until=None)) &
                    (Q(valid_from__lte=timezone.now().date()) | Q(valid_from=None))
                )
                set_to_cache([mac, username], machine, now)
                return machine
            except (Machine.DoesNotExist, ValueError):
                pass
            # User not found, on essaye une multi machine
            try:
                ap = get_nas(data)
                machine = MultiMachine.objects.get(
                    Q(name=username) & Q(active=True) & Q(wifi=True) & Q(vlan=ap.vlan) &
                    (Q(valid_until__gte=timezone.now().date()) | Q(valid_until=None)) &
                    (Q(valid_from__lte=timezone.now().date()) | Q(valid_from=None))
                )
                set_to_cache([mac, username], machine, now)
                return machine
            except (MultiMachine.DoesNotExist, ValueError):
                pass

    radiusd.radlog(radiusd.L_ERR, '[python] Nobody found maching user %s with mac address %s' % (username, mac))
    set_to_cache([mac, username], None, now, null=True)
    return

def register_mac(data, machine):
    """Enregistre la mac actuelle sur une machine donnée."""
    mac = data.get('Calling-Station-Id', None)
    if mac is None:
        radiusd.radlog(radiusd.L_ERR, '[python] Cannot find MAC')
        return
    mac = mac.decode('ascii', 'ignore').replace('"','')
    try:
        mac = format_mac(mac)
    except:
        radiusd.radlog(radiusd.L_ERR, '[python] Cannot format MAC !')
        return

    if isinstance(machine, Machine):
        machine.mac_address = mac
    else:
        machine.add_mac_address_wifi(mac)
    machine.last_connection = timezone.now()
    machine.save()
    radiusd.radlog(radiusd.L_AUTH, '[python] Registering mac address %s for user %s' % (mac, machine.name))

def rlog(msg, ldebug=False, typ=radiusd.L_AUTH):
    if ldebug:
        if not debug:
            return
    radiusd.radlog(typ, '[python] %s' % msg)

@radius_event
def authorize_wifi(data):
    """Section authorize pour le wifi
    (NB: le filaire est en accept pour tout le monde)
    Éxécuté avant l'authentification proprement dite. On peut ainsi remplir les
    champs login et mot de passe qui serviront ensuite à l'authentification
    (MschapV2/PEAP ou MschapV2/TTLS)"""
    rlog('authorize_wifi', ldebug=True)
    
    rlog('Get machine', ldebug=True)
    machine = get_machine(data)

    if not machine:
        rlog('no machine found', typ=radiusd.L_ERR)
        return radiusd.RLM_MODULE_NOTFOUND
    else:
        rlog('got machine "%s"' % machine.name, ldebug=True)

    if not getattr(machine, 'wifi_password', None):
        radiusd.radlog(radiusd.L_ERR, '[python] WiFi authentication but machine has no' +
            'password')
        return radiusd.RLM_MODULE_REJECT

    password = str(machine.wifi_password)

    # TODO: feed cert here
    return (radiusd.RLM_MODULE_UPDATED,
        (),
        (("Cleartext-Password", password),),
      )


@radius_event
def post_auth_wifi(data):
    """Appelé une fois que l'authentification est ok.
    On peut rajouter quelques éléments dans la réponse radius ici.
    Comme par exemple le vlan sur lequel placer le client"""
    rlog('post_auth_wifi', ldebug=True)

    machine = get_machine(data)
    if not isinstance(machine, Machine) or not machine.mac_address:
        register_mac(data, machine)
    elif not machine.last_connection or timezone.now() - machine.last_connection > datetime.timedelta(seconds=CACHE_TIMEOUT):
        machine.last_connection = timezone.now()
        machine.save()
    mac = data.get('Calling-Station-Id', None)

    wap_ip = data.get('NAS-IP-Address')
    try:
       wap_mac = format_mac(data.get('Called-Station-Id').decode('ascii', 'ignore').replace('"',''))
    except:
        wap_mac=None

    log_message = '[python] (wifi) %s [%s] logged in from ap %s [%s]' % \
      (machine.name, format_mac(mac), wap_ip, wap_mac)
    #logger.info(log_message)
    radiusd.radlog(radiusd.L_AUTH, log_message)

    # WiFi : Pour l'instant, on ne met pas d'infos de vlans dans la réponse
    # les bornes wifi ont du mal avec cela
    return radiusd.RLM_MODULE_OK

@radius_event
def dummy_fun(p):
    rlog('dummy_fun', ldebug=True)
    return radiusd.RLM_MODULE_OK

def detach(p=None):
    """Appelé lors du déchargement du module (enfin, normalement)"""
    rlog('detach', ldebug=True)
    print "*** goodbye from auth.py ***"
    return radiusd.RLM_MODULE_OK

