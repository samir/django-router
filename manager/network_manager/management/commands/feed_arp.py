# ⁻*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.utils import timezone
import scapy.layers.l2
import scapy.route
from optparse import make_option
import netaddr
import datetime

from network_manager.models import Machine, Vlan
from network_manager.utils import net_to_iface, format_mac
import network_manager.config as config

class Command(BaseCommand):
    args = ''
    help = u'Met à jour la date de dernière connexion des machines fixes'

    def add_arguments(self, parser):
        parser.add_argument('--debug',
            action='store_true',
            dest='debug',
            default=False,
            help='Debug'),
        parser.add_argument('--incremental',
            action='store_true',
            dest='incremental',
            default=False,
            help='incremental'),

    def handle(self, *args, **options):
        debug = options.get('debug', False)
        incremental = options.get('incremental', False)
        if incremental:
            machines = Machine.objects.filter(~Q(ipv4_address=None) & Q(last_connection__lt=(timezone.now() - datetime.timedelta(0, config.arp_scan_update_interval + 60))))
            if debug:
                print machines
            if machines:
                ip_to_machine = dict((str(m.ipv4_address),m) for m in machines)
                if debug:
                    print ip_to_machine.keys()
                ans, unans = scapy.layers.l2.arping(ip_to_machine.keys(), timeout=3, verbose=debug)
                ips_d = {}
                for s, r in ans.res:
                    ips_d[r.sprintf("%ARP.psrc%")]=r.sprintf("%Ether.src%")
                where = Q(pk=-1)
                for ip, mac in ips_d.items():
                    if mac == ip_to_machine[ip].mac_address:
                        where = where | Q(pk=ip_to_machine[ip].pk)
                machines = Machine.objects.filter(where).update(last_connection = timezone.now())
                if debug:
                    print machines
            return
        for vlan in Vlan.objects.all():
            if vlan.interface and vlan.interface.uplink:
                continue
            if vlan.network_ipv4 and net_to_iface(vlan.network_ipv4):
                if debug:
                    print "Scanning net %s on interface %s" % (vlan.network_ipv4, net_to_iface(vlan.network_ipv4)[0])
                macs_d={}
                ans, unans = scapy.layers.l2.arping(str(vlan.network_ipv4), iface=net_to_iface(vlan.network_ipv4)[0], timeout=3, verbose=debug)
                for s, r in ans.res:
                    if debug:
                        print "%s %s" % (r.sprintf("%Ether.src%"), r.sprintf("%ARP.psrc%"))
                    macs_d[format_mac(r.sprintf("%Ether.src%"))]=r.sprintf("%ARP.psrc%")
                macs = macs_d.keys() # + [format_mac(config.local_mac)] # power
                where=Q(pk=-1)
                for mac in macs:
                    where = where | Q(mac_address=mac)
                machines = Machine.objects.filter(Q(vlan=vlan) & (where))
                machines_mac = set(format_mac(m.mac_address) for m in machines)
                # parfois la mac null pop, et elle ne nous interesse pas
                new_macs = set(macs).difference(machines_mac).difference(['00:00:00:00:00:00'])
                Machine.objects.filter(Q(vlan=vlan) & (where)).update(last_connection = timezone.now())
                for mac in new_macs:
                    ipv4 = macs_d[mac]
                    if netaddr.IPAddress(ipv4) in netaddr.IPNetwork(vlan.network_ipv4):
                        if debug:
                            print "new machine %s %s" % (mac, ipv4)
                        Machine.new(mac, ipv4, vlan=vlan.id)
                #self.stdout.write(str(macs))
