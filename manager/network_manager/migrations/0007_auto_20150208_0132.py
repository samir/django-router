# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import network_manager.fields


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0006_auto_20141020_1956'),
    ]

    operations = [
        migrations.CreateModel(
            name='PortRangeRedirection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('port_range_start', network_manager.fields.IntegerRangeField(null=True, verbose_name=b'premier port', blank=True)),
                ('port_range_end', network_manager.fields.IntegerRangeField(null=True, verbose_name=b'dernier port', blank=True)),
                ('protocol', models.CharField(max_length=3, verbose_name=b'protocole', choices=[(b'udp', b'udp'), (b'tcp', b'tcp')])),
                ('private_machine', models.ForeignKey(related_name='+', verbose_name=b'machine priv\xc3\xa9e', to='network_manager.Machine')),
                ('public_machine', models.ForeignKey(related_name='+', verbose_name=b'machine publique', to='network_manager.Machine')),
            ],
            options={
                'verbose_name': 'redirection de plage de ports en ipv4',
                'verbose_name_plural': 'redirections de plages de ports en ipv4',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='portrangeredirection',
            unique_together=set([('public_machine', 'port_range_start', 'port_range_end', 'protocol')]),
        ),
    ]
