# ⁻*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from network_manager.models import Machine

class Command(BaseCommand):
    args = ''
    help = u'Génère une conf pour les lease statique du dhcp'

    def handle(self, *args, **options):
        stdout, stderr = Machine.update_dhcp()
        if stdout:
            self.stdout.write(stdout)
        if stderr:
            self.stderr.write(stderr)
