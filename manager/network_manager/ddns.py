# ⁻*- coding: utf-8 -*-
import config

import netaddr

import dns.query
import dns.tsigkeyring
import dns.update
import sys

def update_dns(zone, host, ttl, rtype, data=None):
    if data:
        data = str(data)
    key = open(config.ddns_key_path).read()
    keyring = dns.tsigkeyring.from_text({key.split('key ')[1].split(' {')[0] : key.split('secret "')[1].split('";')[0]})

    update = dns.update.Update(zone, keyring=keyring)
    if data:
        update.replace(host, ttl, rtype, data)
        print "setting %s.%s -> %s" % (host, zone, data)
    else:
        update.replace(host, ttl, rtype)
        print "deleting %s.%s %s record" % (host, zone, rtype)

    response = dns.query.tcp(update, config.ddns_server)
    return response

def reverse(net, ip=None):
    """Renvoie la zone DNS inverse correspondant au réseau et à
    l'adresse donnés, ainsi que le nombre d'éléments de l'ip a
    mettre dans le fichier de zone si elle est fournie, n'importe
    quoi sinon."""
    n = netaddr.IPNetwork(net)
    a = netaddr.IPAddress(str(ip) if ip else n.ip)
    rev_dns_a = a.reverse_dns.split('.')[:-1]
    assert a in n
    if n.version == 4:
        if n.prefixlen == 8:
            return ('.'.join(rev_dns_a[3:]), 3, a if ip else None)
        elif n.prefixlen == 16:
            return ('.'.join(rev_dns_a[2:]), 2, a if ip else None)
        elif n.prefixlen == 24:
            return ('.'.join(rev_dns_a[1:]), 1, a if ip else None)
        else:
            return ('.'.join(rev_dns_a[1:]), 1, a if ip else None)
            raise ValueError("Bad network %s" % n)
    elif n.version == 6:
        return ('.'.join(rev_dns_a[(128-n.prefixlen)/4:]), (128-n.prefixlen)/4, a if ip else None)

def mac_to_ipv6(ipv6_prefix, mac_address):
    """Convert a MAC address (EUI48) to an IPv6 (prefix::EUI64)."""
    if mac_address == '<automatique>':
        return ''

    if type(mac_address) in [str, unicode]:
        mac_address = netaddr.EUI(mac_address)
    addr = int(mac_address.bits(netaddr.mac_bare), 2)
    ip6addr = (((addr >> 24) ^ (1 << 17)) << 40) | (0xFFFE << 24) | (addr & 0xFFFFFF)
    n = netaddr.IPNetwork(ipv6_prefix)
    return str(netaddr.IPAddress(n.first + ip6addr))

