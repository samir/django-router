#!/bin/bash
FWDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
iptables(){
    /sbin/iptables $@
}
ip6tables(){
    /sbin/ip6tables $@
}
ipall(){
    /sbin/iptables $@
    /sbin/ip6tables $@
}

echo "Saving sshguard config"
$FWDIR/firewall-sshguard save

echo "Disabling forwarding"
echo "0" > /proc/sys/net/ipv4/ip_forward
echo "0" > /proc/sys/net/ipv6/conf/all/forwarding

echo "Flushing rules"
ipall -F
ipall -t mangle -F
iptables -t nat -F

echo "Packet logging"
ipall    -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j NFLOG --nflog-group 42 --nflog-threshold 20 #--nflog-range 70
ipall    -t mangle -A PREROUTING ! -p tcp -m state --state NEW -j NFLOG --nflog-group 42 --nflog-threshold 20 #--nflog-range 70

echo "Enabling forwarding"
echo "1" > /proc/sys/net/ipv4/ip_forward
echo "1" > /proc/sys/net/ipv6/conf/all/forwarding

/usr/sbin/service radvd restart

echo "Calling django generation script"
/usr/scripts/manager/manage.py  iptables_restore
echo "Restoring sshguard config"
$FWDIR/firewall-sshguard restore

for chain in NAT_PORT_REDIRECT NAT_RANGE_PORT_REDIRECT; do
    iptables -N $chain &>/dev/null
    iptables -t mangle -N $chain &>/dev/null
    iptables -t nat -N $chain &>/dev/null
done

echo "INPUT chain"
ipall     -A INPUT -i lo -j ACCEPT
iptables  -A INPUT -p icmp -j RETURN
ip6tables -A INPUT -p ipv6-icmp -j RETURN
ipall     -A INPUT -j sshguard
ipall     -A INPUT -i tap+ -j ACCEPT
ipall     -A INPUT -i tun+ -j ACCEPT
ipall     -A INPUT -i tinc -j ACCEPT
# make mysql only avaible locally and throught vpn
ipall     -A INPUT -i eth0.2 -p tcp --dport 3306 -j REJECT
# make monit only avaible locally and throught vpn
ipall     -A INPUT -i eth0.2 -p tcp --dport 2812 -j REJECT
# make samba only avaible locally and throught vpn
ipall     -A INPUT -i eth0.2 -p tcp -m multiport --dports 139,445 -j REJECT
iptables  -A INPUT -j NAT_PORT_REDIRECT
iptables  -A INPUT -j NAT_RANGE_PORT_REDIRECT
ipall     -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables  -A INPUT -i eth0.4 -s 192.168.0.0/24 -j RETURN
iptables  -A INPUT -i eth0.3 -s 192.168.3.0/24 -j RETURN
ip6tables -A INPUT -s 2a01:e35:2e38:20f0::/61 -j RETURN
ip6tables -A INPUT -s fe80::/64 -j RETURN
ip6tables -A INPUT -s ff00::/8 -j RETURN
ipall     -A INPUT -p tcp -m multiport --dports 22,53,80,443,4949 -j RETURN
ipall     -A INPUT -p udp -m multiport --dports 53,7,9 -j RETURN
ipall     -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
ipall     -A INPUT -j REJECT


echo "OUTPUT chain"
ipall     -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o eth0.2 -d 82.227.130.255 -j DROP
# INGESS FILTERING
iptables -A OUTPUT -o eth0.2 -d 10.0.0.0/8 -j REJECT
iptables -A OUTPUT -o eth0.2 -d 192.168.0.0/16 -j REJECT
iptables -A OUTPUT -o eth0.2 -s 10.0.0.0/8 -j REJECT
iptables -A OUTPUT -o eth0.2 -s 192.168.0.0/16 -j REJECT

echo "FORWARD chain"
ipall     -A FORWARD -i lo -j ACCEPT
ipall     -A FORWARD -j sshguard
ipall     -A FORWARD -i tap+ -j ACCEPT
ipall     -A FORWARD -i tun+ -j ACCEPT
ipall     -A FORWARD -i tinc -j ACCEPT
ipall     -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
ipall     -A FORWARD -i eth0.3 -o tun+ -j REJECT
ipall     -A FORWARD -i eth0.3 -o tap+ -j REJECT
ipall     -A FORWARD -i eth0.3 -o tinc -j REJECT
ip6tables -A FORWARD -d mb3admin.com -j REJECT
#ipall     -A FORWARD -j MAC_IP
iptables  -A FORWARD ! -i eth0.2 -p tcp -m multiport --dports 80,443 -m mark --mark $($FWDIR/ipt_mark mac_ip_multi) -j RETURN
ipall     -A FORWARD -m mark --mark $($FWDIR/ipt_mark mac_ip_multi) -j REJECT
iptables  -A FORWARD -d 82.227.130.15 -j ACCEPT
iptables  -A FORWARD -s 82.227.130.15 -j ACCEPT
iptables  -A FORWARD -d 212.27.38.253 -j ACCEPT
iptables  -A FORWARD -s 212.27.38.253 -j ACCEPT
ipall     -A FORWARD -j MACHINES
iptables  -A FORWARD -j NAT_PORT_REDIRECT
iptables  -A FORWARD -j NAT_RANGE_PORT_REDIRECT
ipall     -A FORWARD -j VLAN # return outgoing, reject incomming


ipall     -A FORWARD -j RETURN

echo "Clamp MSS"
# MTU 1500
#   ping  -M do -n -s 1472 212.27.48.10
#   => MSS 1500 - 20 - 20 = 1460
iptables    -t mangle -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1460
# MTU 1480
#   ping6  -M do -n -s 1432 free.fr
#   => MSS: 1480 - 40 - 20 = 1420
ip6tables   -t mangle -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1420

echo "Marquage des paquets non autorisé"
ipall    -t mangle -A PREROUTING ! -i eth0.2 -j MAC_IP
ipall    -t mangle -A PREROUTING ! -i eth0.2 -m mark --mark $($FWDIR/ipt_mark mac_ip) -j MAC_IP_MULTI
echo "Marking for lan MASQUERADE"
iptables -t mangle -A PREROUTING ! -i eth0.2 -j NAT_PORT_REDIRECT
iptables -t mangle -A PREROUTING ! -i eth0.2 -j NAT_RANGE_PORT_REDIRECT

echo "nat rules"
iptables -t nat -A PREROUTING -p udp -m multiport --dports 7,9 -j ACCEPT
iptables -t nat -A PREROUTING ! -i eth0.2 ! -d 192.168.0.4 -p tcp --dport 80 -m mark --mark $($FWDIR/ipt_mark mac_ip_multi) -j DNAT --to-destination 192.168.0.4:81
iptables -t nat -A PREROUTING ! -i eth0.2 ! -d 192.168.0.4 -p tcp --dport 443 -m mark --mark $($FWDIR/ipt_mark mac_ip_multi) -j DNAT --to-destination 192.168.0.4:444
iptables -t nat -I PREROUTING -d mb3admin.com -p tcp --dport 443 -j DNAT --to $(dig +short power.genua.fr):8443
iptables -t nat -A PREROUTING -m mark --mark $($FWDIR/ipt_mark mac_ip_multi) -j NFLOG
iptables -t nat -A PREROUTING -j NAT_PORT_REDIRECT
iptables -t nat -A PREROUTING -j NAT_RANGE_PORT_REDIRECT
iptables -t nat -A POSTROUTING -m mark --mark $($FWDIR/ipt_mark masquerade) -j MASQUERADE
iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -d 192.168.3.50 -j MASQUERADE
iptables -t nat -A POSTROUTING -o eth0.2  -j MASQUERADE


