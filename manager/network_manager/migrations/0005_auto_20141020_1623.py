# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0004_auto_20141017_1828'),
    ]

    operations = [
        migrations.CreateModel(
            name='Download',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mac_dst', models.CharField(max_length=17)),
                ('vlan', models.IntegerField()),
                ('ip_dst', models.CharField(max_length=49)),
                ('ip_proto', models.CharField(max_length=10)),
                ('packets', models.IntegerField()),
                ('bytes', models.BigIntegerField()),
                ('stamp_inserted', models.DateTimeField()),
                ('stamp_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'download',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Upload',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mac_src', models.CharField(max_length=17)),
                ('vlan', models.IntegerField()),
                ('ip_src', models.CharField(max_length=49)),
                ('ip_proto', models.CharField(max_length=10)),
                ('packets', models.IntegerField()),
                ('bytes', models.BigIntegerField()),
                ('stamp_inserted', models.DateTimeField()),
                ('stamp_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'upload',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='upload',
            unique_together=set([('mac_src', 'vlan', 'ip_src', 'ip_proto', 'stamp_inserted')]),
        ),
        migrations.AlterUniqueTogether(
            name='download',
            unique_together=set([('mac_dst', 'vlan', 'ip_dst', 'ip_proto', 'stamp_inserted')]),
        ),
        migrations.AlterField(
            model_name='ipv4address',
            name='vlan',
            field=models.ForeignKey(related_name=b'ipv4_addresses', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='network_manager.Vlan', null=True),
        ),
        migrations.AlterField(
            model_name='machine',
            name='owner',
            field=models.ForeignKey(related_name=b'machines', on_delete=django.db.models.deletion.PROTECT, blank=True, to='network_manager.Owner', help_text=b'Si d\xc3\xa9finie, fixe la limite de validit\xc3\xa9 de la machine \xc3\xa0 celle du propr\xc3\xa9taire', null=True, verbose_name=b'propri\xc3\xa9taire'),
        ),
        migrations.AlterField(
            model_name='machine',
            name='vlan',
            field=models.ForeignKey(related_name=b'machines', on_delete=django.db.models.deletion.PROTECT, to='network_manager.Vlan', help_text=b"En changeant le vlan, l'adresse ip de la machine est r\xc3\xa9initialis\xc3\xa9e"),
        ),
    ]
