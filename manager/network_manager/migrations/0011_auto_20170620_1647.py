# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-20 14:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0010_auto_20170613_1718'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipv4address',
            name='ip',
            field=models.GenericIPAddressField(unique=True),
        ),
    ]
