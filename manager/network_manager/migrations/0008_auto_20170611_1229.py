# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import network_manager.utils
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0007_auto_20150208_0132'),
    ]

    operations = [
        migrations.CreateModel(
            name='MacAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mac_address', models.CharField(max_length=18, null=True, verbose_name=b'Adresse mac', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MultiMachine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"Nom d'utilisateur utilis\xc3\xa9 pour la connexion au r\xc3\xa9seau WiFi.", max_length=255, verbose_name=b'nom')),
                ('wifi', models.BooleanField(default=False, help_text=b'Peut-on se connecter en wifi', verbose_name=b'wifi')),
                ('fixe', models.BooleanField(default=False, help_text=b'Peut-on se connecter en filaire. Une seule multi-machine peut \xc3\xaatre activ\xc3\xa9 en m\xc3\xaame temps par vlan. Toutes nouvelle machine se connectant en filaire y sera automatiquement ajout\xc3\xa9.', verbose_name=b'fixe')),
                ('active', models.BooleanField(default=True, verbose_name=b'active')),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name=b'cr\xc3\xa9ation')),
                ('last_modified', models.DateTimeField(auto_now=True, verbose_name=b'derni\xc3\xa8re modification')),
                ('last_connection', models.DateTimeField(help_text=b"Derni\xc3\xa8re fois qu'une machine a \xc3\xa9t\xc3\xa9 vue sur le r\xc3\xa9seau.", null=True, verbose_name=b'derni\xc3\xa8re connexion', blank=True)),
                ('valid_until', models.DateField(help_text=b"Date jusqu'\xc3\xa0 laquelle on peut s'authentifier. Laissez vide pour pas de limite.", null=True, verbose_name=b'limite de validit\xc3\xa9', blank=True)),
                ('wifi_password', models.CharField(default=network_manager.utils.random_wifi_password, help_text=b'G\xc3\xa9n\xc3\xa9r\xc3\xa9 al\xc3\xa9atoirement, ne peut \xc3\xaatre modifi\xc3\xa9.', unique=True, max_length=255, verbose_name=b'mot de passe WiFi')),
                ('owner', models.ForeignKey(related_name='multi_machines', on_delete=django.db.models.deletion.PROTECT, blank=True, to='network_manager.Owner', help_text=b'Si d\xc3\xa9finie, fixe la limite de validit\xc3\xa9 de la machine \xc3\xa0 celle du propr\xc3\xa9taire', null=True, verbose_name=b'propri\xc3\xa9taire')),
                ('vlan', models.ForeignKey(related_name='multi_machines', on_delete=django.db.models.deletion.PROTECT, to='network_manager.Vlan')),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='multimachine',
            unique_together=set([('name', 'vlan')]),
        ),
        migrations.AddField(
            model_name='macaddress',
            name='machine',
            field=models.ForeignKey(related_name='mac_addresses', to='network_manager.MultiMachine'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='macaddress',
            unique_together=set([('mac_address', 'machine')]),
        ),
    ]
