# ⁻*- coding:  utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.core.validators import EmailValidator
from django.db.models import Q
from django.utils import timezone
from django.core.mail import send_mail

import re
import requests
import datetime
import operator
from unidecode import unidecode

from network_manager.models import Owner, Vlan, MultiMachine
from network_manager.forms import MultiMachineForm, OwnerForm
import network_manager.utils as utils

_phone_regexp = re.compile(r'^\+?[\d()-]{8,15}$')
_email_validator = EmailValidator()

def _create_multi_machine(owner, event):
    studio_vlan = Vlan.objects.get(name="studio")
    MultiMachineForm.check_dates(studio_vlan, owner.valid_from, owner.valid_until)
    names = [unidecode(n) for n in event["name"].strip().lower().split()]
    try:
        name = names[0]
        return owner.multi_machines.create(name=name, fixe=True, wifi=True, vlan=studio_vlan)
    except IntegrityError:
        pass
    try:
        name = names[-1]
        return owner.multi_machines.create(name=name, fixe=True, wifi=True, vlan=studio_vlan)
    except IntegrityError:
        pass
    try:
        name = u"%s%s" % (names[0][0], names[-1])
        return owner.multi_machines.create(name=name, fixe=True, wifi=True, vlan=studio_vlan)
    except IntegrityError:
        pass
    try:
        name = u"%s%s" % (names[-1][0], names[0])
        return owner.multi_machines.create(name=name, fixe=True, wifi=True, vlan=studio_vlan)
    except IntegrityError:
        pass
    try:
        name = u"%s_%s" % (names[0], names[-1])
        return owner.multi_machines.create(name=name, fixe=True, wifi=True, vlan=studio_vlan)
    except IntegrityError:
        pass
    i = 0
    while True:
        try:
            name = u"%s%d" % (names[0], i)
            return owner.multi_machines.create(name=name, fixe=True, wifi=True, vlan=studio_vlan)
        except IntegrityError:
            pass
        i += 1

class Command(BaseCommand):
    args = ''
    help = u'Met à jour la date de dernière connexion des machines fixes'

    def add_arguments(self, parser):
        parser.add_argument('url',
            help='Url of an airbnb calendar export'
        ),

    def handle(self, *args, **options):
        url = options['url']
        events = self._fetch(url)
        machines = []
        for event in events:
            owner = Owner.objects.get_or_create(
                name=event["name"],
                email=event["email"],
                phone_number=event["phone"],
                defaults={'airbnb': True}
            )[0]
            need_save = False
            need_mail = False
            if owner.valid_from is None:
                owner.valid_from = event["start"]
                need_save = True
            if owner.valid_until is None:
                owner.valid_until = event["end"]
                need_save = True
            if owner.valid_until < timezone.now().date():
                if owner.valid_until <= event["start"]:
                    if owner.valid_from < event["start"]:
                        owner.valid_from = event["start"]
                        need_save = True
                    if owner.valid_until < event["end"]:
                        owner.valid_until = event["end"]
                        need_save = True
            if need_save:
                OwnerForm.check_dates(owner, owner.valid_from, owner.valid_until)
                owner.save()
                need_mail = True
            multi_machines = owner.multi_machines.all()
            if len(multi_machines) > 1:
                raise ValueError("Plus d'une multi machine pour %s" % owner.name)
            elif len(multi_machines) < 1:
                machine = _create_multi_machine(owner, event)
                need_mail = True
            else:
                machine = multi_machines[0]
            machines.append(machine)
            if need_mail:
                send_mail(
                    u'Mot de passe wifi de %s' % owner.name,
                    (
                        u'Bonjour,\n\nLes identifiant wifi de %s sont les suivant :\n\n'
                        u"Nom d'utilisateur: %s\nMot de passe: %s\n\n"
                        u"Pour information, %s à loué le studio du %s au %s.\n\n"
                        u"Cordialement,\n\n"
                        u'-- \nLe programme de gestion du réseau'
                    ) % (owner.name, machine.name, machine.wifi_password, owner.name, event["checkin"], event["checkout"]),
                    'network-manager@cmg.genua.fr',
                    ['valentin@samir.re', 'anne@martinez.pm'],
                    fail_silently=False,
                )
        if machines:
            ret1 = MultiMachine.objects.filter(reduce(operator.and_, [~Q(id=machine.id) for machine in machines]) | Q(valid_until__lt=timezone.now().date())).update(active=False)
            ret2 = MultiMachine.objects.filter(reduce(operator.or_, [Q(id=machine.id) for machine in machines]) & Q(valid_until__gte=timezone.now().date())).update(active=True)
            if (ret1 + ret2) > 0 or need_save:
                utils.iptables_restore()
        for machine in MultiMachine.objects.filter(active=False):
            if len(machine.mac_addresses) == 0:
                machine.delete()
        for owner in Owner.objects.filter(airbnb=True):
            if len(owner.multi_machines.all()) == 0 and len(owner.machines.all()) == 0:
                owner.delete()

    def _validates(self, data):
        events = []
        for event in data:
            try:
                event["nights"] = int(event["nights"])
                event["start"] = datetime.datetime.strptime(event["start"], "%Y%m%d").date()
                event["end"] = datetime.datetime.strptime(event["end"], "%Y%m%d").date()
                if event["start"] - datetime.datetime.now().date() > datetime.timedelta(1) or event["start"] < (datetime.datetime.now().date() - datetime.timedelta(1)):
                    continue
                if len(event["checkin"]) < 8:
                    raise ValueError("Bad checkin date")
                if len(event["checkout"]) < 8:
                    raise ValueError("Bad checkout date")
                if len(event["name"]) < 1:
                    raise ValueError("Name too short")
                if len(event["booking_nb"]) < 10:
                    raise ValueError("Booking number too short")
                if not _phone_regexp.match(event["phone"]):
                    raise ValueError("Bad phone number: %r" % event["phone"])
                _email_validator(event["email"])
                events.append(event)
            except (ValueError, ValidationError) as error:
                print "%r" % error
        return events

    def _fetch(self, url):
        r = requests.get(url)
        events = []
        i = 0
        data = r.text.split('\n')
        length = len(data)
        while i < length:
            if data[i] == "BEGIN:VEVENT":
                i += 1
                event = {}
                while data[i] != "END:VEVENT":
                    if data[i].startswith("DTEND;VALUE=DATE:"):
                        event["end"] = data[i][17:]
                    elif data[i].startswith("DTSTART;VALUE=DATE:"):
                        event["start"] = data[i][19:]
                    elif data[i].startswith("UID:"):
                        event["uid"]= data[i][4:]
                    elif data[i].startswith("DESCRIPTION:"):
                        description = [data[i][12:]]
                        while data[i+1][0] == ' ':
                            i += 1
                            description.append(data[i][1:])
                        event["description"] = u"".join(description).replace(u'\\n', '\n')
                    elif data[i].startswith("SUMMARY:"):
                        event['name'] = data[i][8:-13]
                        event['booking_nb'] = data[i][-11:-1]
                    i += 1
                try:
                    for line in event["description"].split('\n'):
                        if line.startswith("CHECKIN: "):
                            event["checkin"] = line[9:]
                        elif line.startswith("CHECKOUT: "):
                            event["checkout"]= line[10:]
                        elif line.startswith("NIGHTS: "):
                            event["nights"] = line[8:]
                        elif line.startswith("PHONE:"):
                            event["phone"] = line[6:].replace(" ", "")
                        elif line.startswith("EMAIL: "):
                            event["email"] = line[7:]
                    del event["description"]
                    events.append(event)
                except KeyError:
                    pass
            i += 1
        return self._validates(events)
