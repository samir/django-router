# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('network_manager', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('address', models.CharField(max_length=255)),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name=b'cr\xc3\xa9ation')),
                ('valid_until', models.DateField(help_text=b"Date jusqu'\xc3\xa0 laquelle les machines de l'utilisateur peuvent s'authentifier. Laissez vide pour pas de limite.", null=True, verbose_name=b'limite de validit\xc3\xa9', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='machine',
            name='owner',
            field=models.ForeignKey(related_name=b'machines', blank=True, to='network_manager.Owner', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ipv4address',
            name='vlan',
            field=models.ForeignKey(related_name=b'ipv4_addresses', blank=True, to='network_manager.Vlan', null=True),
        ),
        migrations.AlterField(
            model_name='machine',
            name='vlan',
            field=models.ForeignKey(related_name=b'machines', to='network_manager.Vlan', help_text=b"En changeant le vlan, l'adresse ip de la machine est r\xc3\xa9initialis\xc3\xa9e"),
        ),
    ]
