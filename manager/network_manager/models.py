# ⁻*- coding: utf-8 -*-
from django.conf import settings
from django.db import models, connection, IntegrityError
from django.db.models import Sum, Q
from django.core import urlresolvers
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.core.validators import RegexValidator

import time
import pytz
import datetime
import scapy.config
import netaddr
import socket
import os
import hashlib
from subprocess import Popen, PIPE
from netfilter.rule import Rule, Match, Target
import copy 

import fields
import config
import ddns

iptables_classes = []

from utils import random_wifi_password, run_once, iptables_restore, has, dictfetchall, cache, iptables, port_name_to_int

phone_regex = RegexValidator(regex=r'^\+?\d{8,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")

def produce_firewall(classe):
    global iptables_classes
    iptables_classes.append(classe)
    return classe

class BufferMessages(models.Model):
    class Meta:
        abstract = True

    _messages = []


    def add_message(self, *args):
        if args not in self._messages:
            self._messages.append(args)

    def enqueue_messages(self, request):
        try:
            while True:
                (msg_type, msg_data) = self._messages.pop()
                messages.add_message(request, msg_type, msg_data)
        except IndexError:
            pass

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))

class TrackChanges(BufferMessages):
    class Meta:
        abstract = True

    _original_values = {}
    _has_change = []

    def __init__(self, *args, **kwargs):
        super(TrackChanges, self).__init__(*args, **kwargs)
        self._initialise_original_values()

    def _initialise_original_values(self):
        self._has_change = []
        self._original_values = {}
        for field in self._meta.get_fields():
            try:
                self._original_values[field.name] = getattr(self, field.name)
            except:
                self._original_values[field.name] = None

    def _fields_operations(self):
        for field in self._meta.get_fields():
            if (field.many_to_one and field.related_model is None):
                continue
            # Set to None blank fields
            try:
                if field.blank and field.null:
                    if not getattr(self, field.name) and getattr(self, field.name) is not None and getattr(self, field.name) != False:
                        try:
                            print "Setting %s to None" % field.name
                            setattr(self, field.name, None)
                        except:
                            pass
            except AttributeError:
                pass
            try:
                # call on_change trigger
                if getattr(self, field.name) != self._original_values[field.name]:
                    self._has_change.append(field.name)
                    if getattr(self, "_on_change_%s" % field.name, None) and callable(getattr(self, "_on_change_%s" % field.name)):
                        print "Calling _on_change_%s" % field.name
                        getattr(self, "_on_change_%s" % field.name)()
            except AttributeError:
                pass

class Owner(TrackChanges):
    class Meta:
        verbose_name = "Propriétaire"
        unique_together = (("name", "phone_number", "email"), )
    name = models.CharField(max_length=255, verbose_name="nom")
    address = models.CharField(max_length=255, blank=True, null=True, verbose_name="adresse")
    date_added = models.DateTimeField(auto_now_add=True, verbose_name="création")
    valid_until = models.DateField(null=True, blank=True, verbose_name="limite de validité", help_text="Date jusqu'à laquelle les machines de l'utilisateur peuvent s'authentifier. Laissez vide pour pas de limite ou pour mettre des limites par machine.")
    valid_from = models.DateField(null=True, blank=True, verbose_name="Valide à partir de", help_text="Date à partir de laquelle on peut s'authentifier. Laissez vide pour pas de limite.")
    phone_number = models.CharField(max_length=16, validators=[phone_regex], blank=True, verbose_name="Numéro de téléphone", help_text="De préférence format +NNNNNNNNNNN")
    email = models.EmailField(verbose_name="Email", blank=True, null=True)
    airbnb = models.BooleanField(default=False, verbose_name="Airbnb", help_text="La personne vient-elle via airbnb ?")

    def __unicode__(self):
        return self.name

    def _on_change_valid_until(self):
        self.machines.update(valid_until=self.valid_until)
        self.multi_machines.update(valid_until=self.valid_until)

    def _on_change_valid_from(self):
        self.machines.update(valid_from=self.valid_from)
        self.multi_machines.update(valid_from=self.valid_from)

    def save(self, *args, **kwargs):
        self._fields_operations()
        # call parent method to save the object
        ret = super(Owner, self).save(*args, **kwargs)
        self._initialise_original_values()
        return ret

class Interface(BufferMessages):
    name = models.CharField(max_length=255)
    uplink = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.uplink:
            Interface.objects.all().update(uplink=False)
        super(Interface, self).save(*args, **kwargs)

@produce_firewall
class Vlan(BufferMessages):
    class Meta:
        ordering = ("id", )
    id = models.IntegerField(unique=True, primary_key=True)
    name = models.CharField(max_length=255, verbose_name="nom", unique=True)
    network_ipv4 = models.CharField(max_length=255, blank=True, null=True)
    network_ipv6 = models.CharField(max_length=255, blank=True, null=True)
    interface = models.OneToOneField(Interface, related_name='vlan', blank=True, null=True)
    auto_register = models.BooleanField(
        default=False,
        verbose_name="Création automatique de machines",
        help_text=(
            "Si activé, lorsqu'une nouvelle adresse mac aparaît sur le vlan, "
            "une machine correspondante, désactivée est créée."
        )
    )

    def __unicode__(self):
        return u" %s - %s" % (self.id, self.name)
    def iptables_rules_raw(self):
        return ([], [])
    def iptables_rules_mangle(self):
        return ([], [])

    def iptables_rules_filter(self):
        rule=Rule(chain="VLAN")
        if self.interface:
            rule.in_interface = self.interface.name
        rule.jump = Target("RETURN")
        if self.interface and self.interface.uplink:
            return ([], [])
        rulesv4 = []
        rulesv6 = []
        uplinks = [vlan for vlan in Vlan.objects.all() if vlan.interface and vlan.interface.uplink]
        for uplink in uplinks:
            if self.network_ipv4:
                rulev4 = copy.deepcopy(rule)
                rulev4.source = self.network_ipv4
                rulev4.out_interface = uplink.interface.name
                rulesv4.append(rulev4)
            if self.network_ipv6:
                rulev6 = copy.deepcopy(rule)
                rulev6.source = self.network_ipv6
                rulev6.out_interface = uplink.interface.name
                rulesv6.append(rulev6)
        return (rulesv4, rulesv6)

    iptables_rules_filter_extra = ([], [Rule(chain="VLAN", jump=Target("REJECT"))])
    def iptables_rules_nat(self):
        return ([], [])


    def delete(self, *args, **kwargs):
        ret = super(Vlan, self).delete(*args, **kwargs)
        iptables_restore()
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        return ret

    def save(self, *args, **kwargs):
        ret = super(Vlan, self).save(*args, **kwargs)
        iptables_restore()
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        return ret

class Ipv4Address(BufferMessages):
    class Meta:
        ordering = ("bin", )
        verbose_name = "adresse ipv4"
        verbose_name_plural = "adresses ipv4"
    ip = models.GenericIPAddressField(unique=True)
    bin = models.BigIntegerField(unique=True, blank=True, null=True, editable=False)
    public = models.NullBooleanField(verbose_name="adresse publique", blank=True, null=True, editable=False)
    vlan = models.ForeignKey(Vlan, related_name='ipv4_addresses', blank=True, null=True, on_delete=models.SET_NULL)

    def save(self, *args, **kwargs):
        ip = netaddr.IPAddress(self.ip)
        self.bin = long(ip.bin, 2)
        if (ip in config.private_ips_exception and self.public) or self.public == ip.is_private() or self.public is None:
            if ip in config.private_ips_exception:
                self.public = False
            else:
                self.public = not ip.is_private()
            try:
                self.machine.has_public_ipv4_address = self.public
                self.machine.save()
            except Machine.DoesNotExist:
                pass
        if self.vlan and (not self.vlan.network_ipv4 or not ip in netaddr.IPNetwork(self.vlan.network_ipv4)):
            raise ValueError("adresse ip et network du vlan ne correspondent pas")
        super(Ipv4Address, self).save(*args, **kwargs)

    def __unicode__(self):
        return unicode(self.ip)

    def __eq__(self, y):
        return str(self) == str(y)

    def __str__(self):
        return str(self.ip)

@produce_firewall
class MultiMachine(TrackChanges):
    class Meta:
        ordering = ("name", )
        unique_together = (("name", "vlan"), )


    _original_values = {}
    _added_macs = set()

    def __init__(self, *args, **kwargs):
        super(MultiMachine, self).__init__(*args, **kwargs)
        try:
            rule = Rule(chain="MAC_IP_MULTI", in_interface=Interface.objects.get(uplink=True).name, jump=Target("RETURN"))
            if not rule in self.iptables_rules_mangle_extra[0]:
                self.iptables_rules_mangle_extra[0].append(rule)
        except Interface.DoesNotExist:
            pass

    name = models.CharField(max_length=255, verbose_name="nom", help_text="Nom d'utilisateur utilisé pour la connexion au réseau WiFi.")
    wifi = models.BooleanField(default=False, verbose_name="wifi", help_text="Peut-on se connecter en wifi")
    fixe = models.BooleanField(default=False, verbose_name="fixe", help_text=(
        "Peut-on se connecter en filaire. Une seule multi-machine peut être activé en même temps par vlan."
        " Toutes nouvelle machine se connectant en filaire y sera automatiquement ajouté."
    ))
    active = models.BooleanField(default=True, verbose_name="active")
    date_added = models.DateTimeField(auto_now_add=True, verbose_name="création")
    last_modified = models.DateTimeField(auto_now=True, verbose_name="dernière modification")
    last_connection = models.DateTimeField(null=True, blank=True, verbose_name="dernière connexion", help_text="Dernière fois qu'une machine a été vue sur le réseau.")
    valid_until = models.DateField(null=True, blank=True, verbose_name="limite de validité", help_text="Date jusqu'à laquelle on peut s'authentifier. Laissez vide pour pas de limite.")
    valid_from = models.DateField(null=True, blank=True, verbose_name="Valide à partir de", help_text="Date à partir de laquelle on peut s'authentifier si la machine est active. Laissez vide pour pas de limite.")
    # mac_addresses via a ForeignKey
    wifi_password = models.CharField(max_length=255, unique=True, default=random_wifi_password, verbose_name="mot de passe WiFi", help_text="Généré aléatoirement, ne peut être modifié.")
    vlan = models.ForeignKey(Vlan, related_name='multi_machines', on_delete=models.PROTECT)
    owner = models.ForeignKey(Owner, related_name='multi_machines', verbose_name="propriétaire", blank=True, null=True, help_text="Si définie, fixe la limite de validité de la machine à celle du proprétaire", on_delete=models.PROTECT)

    def add_mac_iptables(self, mac):
        cmd = ['-t', 'mangle', '-I', 'MAC_IP_MULTI', '4']
        if self.vlan.interface:
            cmd.extend(['-i', self.vlan.interface.name])
        cmd.extend(['-m', 'mac', '--mac-source', mac, '-j', 'RETURN'])
        iptables(cmd, ipv6=False)
        iptables(cmd, ipv6=True)

    @property
    def mac_addresses(self):
        macs = []
        if self.fixe:
            macs.extend([m.mac_address for m in self.macaddressfixe_set.all()])
        if self.wifi:
            macs.extend([m.mac_address for m in self.macaddresswifi_set.all()])
        return macs

    def add_mac_address_wifi(self, mac):
        try:
            self.macaddresswifi_set.create(mac_address=mac)
            self._added_macs.add(mac)
        except IntegrityError:
            pass

    def add_mac_address_fixe(self, mac):
        try:
            self.macaddressfixe_set.create(mac_address=mac)
            self._added_macs.add(mac)
        except IntegrityError:
            pass

    def has_mac(self, mac):
        return bool(self.macaddressfixe_set.filter(mac_address=mac)) or bool(self.macaddresswifi_set.filter(mac_address=mac))

    def delete(self, *args, **kwargs):
        self._fields_operations()
        ret = super(MultiMachine, self).delete(*args, **kwargs)
        if self.mac_addresses and (self.active or "active" in self._has_change):
            iptables_restore()
            self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        self._initialise_original_values()
        return ret

    def save(self, *args, **kwargs):
        if self.owner:
            self.valid_until = self.owner.valid_until
            self.valid_from = self.owner.valid_from
        if self.active and self.valid_until and self.valid_until < timezone.now().date():
            self.active = False

        self._fields_operations()
        # call parent method to save the object
        ret = super(MultiMachine, self).save(*args, **kwargs)
        if self.mac_addresses and (self.active or "active" in self._has_change):
            for mac in self._added_macs:
                self.add_mac_iptables(mac)
            if has(["vlan", "active", "fixe", "wifi"], self._has_change):
                iptables_restore()
                self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        self._initialise_original_values()
        return ret

    def __unicode__(self):
        if self.active and self.valid_until and self.valid_until < timezone.now().date():
            self.active = False
            self.save()
        return self.name

    def iptables_rules_raw(self):
        return ([], [])

    def iptables_rules_mangle(self):
        rules = []
        if (
            self.active and
            (self.valid_until is None or self.valid_until >= timezone.now().date()) and
            (self.valid_from is None or self.valid_from <= timezone.now().date())
        ):
            for mac in self.mac_addresses:
                rule=Rule()
                rule.chain = "MAC_IP_MULTI"
                if self.vlan.interface:
                    rule.in_interface = self.vlan.interface.name
                rule.jump = Target("RETURN")
                rule.matches.append(Match("mac", "--mac-source %s" % mac))
                rules.append(rule)
        return (rules, rules)
    iptables_rules_mangle_extra = ([Rule(chain="MAC_IP_MULTI", in_interface="tun+", jump=Target("RETURN")), Rule(chain="MAC_IP_MULTI", in_interface="tap+", jump=Target("RETURN"))], [Rule(chain="MAC_IP_MULTI", jump=Target("MARK", '--set-mark %s' % config.ipt_mark['mac_ip_multi']))])

    def iptables_rules_filter(self):
        return ([], [])

    def iptables_rules_nat(self):
        return ([], [])


class MacAddress(models.Model):
    class Meta:
        unique_together = (("mac_address", "machine"), )
        abstract = True


    mac_address = models.CharField(max_length=18, null=True, blank=True, verbose_name="Adresse mac")
    machine = models.ForeignKey(MultiMachine, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.mac_address

class MacAddressFixe(MacAddress):
    pass
class MacAddressWifi(MacAddress):
    pass


@produce_firewall
class Machine(TrackChanges):
    class Meta:
        ordering = ("name", )
        unique_together = (("name", "wifi"), ("mac_address", "vlan"))


    _original_values = {}

    def __init__(self, *args, **kwargs):
        super(Machine, self).__init__(*args, **kwargs)
        try:
            #if not self.iptables_rules_filter_extra[0]:
            #    self.iptables_rules_filter_extra[0].append(Rule(chain="MAC_IP", in_interface=Interface.objects.get(uplink=True).name, jump=Target("RETURN")))
            rule = Rule(chain="MAC_IP", in_interface=Interface.objects.get(uplink=True).name, jump=Target("RETURN"))
            if not rule in self.iptables_rules_mangle_extra[0]:
                self.iptables_rules_mangle_extra[0].append(rule)
        except Interface.DoesNotExist:
            pass

    name = models.CharField(max_length=255, verbose_name="nom", help_text="Nom d'utilisateur utilisé pour la connexion de la machine au réseau WiFi.")
    wifi = models.BooleanField(default=False, verbose_name="wifi", help_text="La machine se connecte-t-elle en wifi ou en filaire")
    active = models.BooleanField(default=True, verbose_name="active") #, help_text="La machine peut-elle s'authentifier ?")
    date_added = models.DateTimeField(auto_now_add=True, verbose_name="création")
    last_modified = models.DateTimeField(auto_now=True, verbose_name="dernière modification")
    last_connection = models.DateTimeField(null=True, blank=True, verbose_name="dernière connexion", help_text="Dernière fois que la machine a été vue sur le réseau.")
    valid_until = models.DateField(null=True, blank=True, verbose_name="limite de validité", help_text="Date jusqu'à laquelle la machine peut s'authentifier. Laissez vide pour pas de limite.")
    valid_from = models.DateField(null=True, blank=True, verbose_name="Valide à partir de", help_text="Date à partir de laquelle on peut s'authentifier si la machine est active. Laissez vide pour pas de limite.")
    mac_address = models.CharField(max_length=18, null=True, blank=True, verbose_name="Adresse mac", help_text="L'adresse mac est assignée automatiquement à la première connexion en wifi et doit être mise à la main en filaire")
    wifi_password = models.CharField(max_length=255, unique=True, default=random_wifi_password, verbose_name="mot de passe WiFi", help_text="Généré aléatoirement, ne peut être modifié.")
    manufacturer = models.CharField(max_length=255, blank=True, null=True, verbose_name="fabricant", help_text="Fabricant de la carte réseau, si connu (trouvé à partir de l'adresse MAC).")
    ipv4_address = models.OneToOneField(Ipv4Address, blank=True, null=True, related_name='machine', verbose_name="adresse ipv4")
    has_public_ipv4_address = models.NullBooleanField(blank=True, null=True)
    vlan = models.ForeignKey(Vlan, related_name='machines', help_text="En changeant le vlan, l'adresse ip de la machine est réinitialisée", on_delete=models.PROTECT)
    owner = models.ForeignKey(Owner, related_name='machines', verbose_name="propriétaire", blank=True, null=True, help_text="Si définie, fixe la limite de validité de la machine à celle du proprétaire", on_delete=models.PROTECT)

    def ipv6_address(self, old=False):
        """Adresse ipv6 canonique de la machine"""
        if old:
            mac_address = self._original_values["mac_address"]
        else:
            mac_address = self.mac_address
        if mac_address and self.vlan.network_ipv6:
            return ddns.mac_to_ipv6(self.vlan.network_ipv6, mac_address)
        else:
            return None

    def ipv6_addresses(self):
        """Liste des adresses ipv6 ayant été utilisé par la machine"""
        if self.mac_address:
            return Download.ipv6_addresses(self)
        else:
            return []

    def dns_name(self, old=False, fqdn=False):
        if old:
            name = self._original_values["name"]
            wifi = self._original_values["wifi"]
        else:
            name = self.name
            wifi = self.wifi
        if wifi:
            name = "%s.wifi" % (name)
        else:
            name = "%s" % (name)
        if fqdn:
            return "%s.%s" % (name, config.ddns_domain)
        else:
            return name

    def _update_dns(self):
        self._update_direct()
        self._update_reverse()

    def _update_direct(self):
        # Mise à jour du dns direct
        if self.ipv6_address():
            ddns.update_dns(config.ddns_domain, self.dns_name(), config.ddns_ttl, 'AAAA', self.ipv6_address())
        else:
            ddns.update_dns(config.ddns_domain, self.dns_name(old=True), config.ddns_ttl, 'AAAA')

        if self.ipv4_address:
            ddns.update_dns(config.ddns_domain, self.dns_name(), config.ddns_ttl, 'A', self.ipv4_address)
        else:
            ddns.update_dns(config.ddns_domain, self.dns_name(old=True), config.ddns_ttl, 'A')
            ddns.update_dns(config.ddns_domain, self.dns_name(old=True), config.ddns_ttl, 'TXT')

        mac_host = "_mac.%s" % self.dns_name()
        if self.mac_address:
            ddns.update_dns(config.ddns_domain, mac_host, config.ddns_ttl, 'TXT', self.mac_address)
        else:
            ddns.update_dns(config.ddns_domain, mac_host, config.ddns_ttl, 'TXT')

    def _update_reverse(self):
        def aux(self, network, old_ip, new_ip):
            # mise à jour du reverse
            if new_ip:
                try:
                    zone, length, ip = ddns.reverse(network, new_ip)
                    nom = '.'.join(ip.reverse_dns.split('.')[:length])
                    ddns.update_dns(zone, nom, config.ddns_ttl, 'PTR', "%s." % self.dns_name(fqdn=True))
                except AssertionError:
                    pass
            elif old_ip:
                try:
                    old_zone, length, ip = ddns.reverse(network, old_ip)
                    old_nom = '.'.join(ip.reverse_dns.split('.')[:length])
                    ddns.update_dns(old_zone, old_nom, config.ddns_ttl, 'PTR')
                except AssertionError:
                    pass


        # mise à jour du reverse
        if self.vlan.network_ipv4:
            aux(self, self.vlan.network_ipv4, self._original_values["ipv4_address"], self.ipv4_address)
        if self.vlan.network_ipv6:
            aux(self, self.vlan.network_ipv6, self.ipv6_address(old=True), self.ipv6_address())

        
    def _update_manufacturer(self):
        self.manufacturer = None
        if self.mac_address:
            vendor = scapy.config.conf.manufdb._get_manuf(self.mac_address)
            if vendor != self.mac_address:
                self.manufacturer = vendor

    def _on_change_name(self):
        self._update_dns()

    def _on_change_ipv4_address(self):
        self._update_dns()
        if self.ipv4_address:
            self.has_public_ipv4_address = self.ipv4_address.public
        else:
            self.has_public_ipv4_address = None

    def _on_change_mac_address(self):
        self._update_dns()
        self._update_manufacturer()

    def _on_change_vlan(self):
        self.firewall_rules.all().update(vlan=self.vlan)

    def delete(self, *args, **kwargs):
        ret = super(Machine, self).delete(*args, **kwargs)
        Machine.update_dhcp()
        self.add_message(messages.SUCCESS, "Configuration dhcp mise à jour")
        iptables_restore()
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        self._initialise_original_values()
        return ret

    def save(self, *args, **kwargs):
        self._update_direct = run_once(self._update_direct)
        self._update_reverse = run_once(self._update_reverse)

        if self.owner:
            self.valid_until = self.owner.valid_until
            self.valid_from = self.owner.valid_from
        if self.active and self.valid_until and self.valid_until < timezone.now().date():
            self.active = False

        self._fields_operations()
        # call parent method to save the object
        ret = super(Machine, self).save(*args, **kwargs)
        if has(["ipv4_address", "mac_address", "name", "wifi"], self._has_change):
            Machine.update_dhcp()
            self.add_message(messages.SUCCESS, "Configuration dhcp mise à jour")
        if has(["ipv4_address", "mac_address", "vlan", "active"], self._has_change):
            iptables_restore()
            self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        self._initialise_original_values()
        return ret

    def __unicode__(self):
        if self.active and self.valid_until and self.valid_until < timezone.now().date():
            self.active = False
            self.save()
        return self.dns_name()

    @classmethod
    def new(cls, mac, ipv4, vlan=1):
        try:
            name = socket.gethostbyaddr(ipv4)[0].split('.', 1)[0]
        except (socket.herror, socket.gaierror):
            name = ipv4
        return cls.new_mac_address(mac, name, vlan=vlan)

    @classmethod
    def new_mac_address(cls, mac_address, name, vlan=1):
        if not isinstance(vlan, Vlan):
            vlan=Vlan.objects.get(id=vlan)
        machine = cls(name="%s-%s-%s" % (name, vlan.name, int(time.time()) % 3600), active=False, wifi=False, vlan=vlan)
        machine.mac_address=mac_address
        machine.save()
        return machine

    @classmethod
    def update_dhcp(cls):
        dhcp_list=""
        for machine in cls.objects.filter(~Q(ipv4_address=None) & ~Q(mac_address=None)):
            dhcp_list+=(config.dhcp_host_template % {'host' : machine.dns_name(fqdn=True), 'mac':machine.mac_address, 'ipv4': machine.ipv4_address, 'hostname' : machine.dns_name()}).encode('utf-8')

        dhcp_md5=hashlib.md5(dhcp_list).hexdigest()
        try:
            dhcp_old_md5=hashlib.md5(open('/etc/dhcp/machine.liste','r').read()).hexdigest()
        except IOError:
            dhcp_old_md5=""
        if dhcp_old_md5 != dhcp_md5:
            file=open('/etc/dhcp/machine.liste.new', 'w')
            file.write(dhcp_list)
            file.close()
            os.rename('/etc/dhcp/machine.liste.new', '/etc/dhcp/machine.liste')
            Popen(['sudo', 'service', 'isc-dhcp-server', 'force-reload'])
            return "",""
        else:
            return "nothing to do", ""


    def iptables_rules_raw(self):
        return ([], [])
    def iptables_rules_mangle(self):
        rule=Rule()
        rule.chain = "MAC_IP"
        if self.vlan.interface:
            rule.in_interface = self.vlan.interface.name
        rule.jump = Target("RETURN")
        if (
            self.mac_address and self.active and
            (self.valid_until is None or self.valid_until >= timezone.now().date()) and
            (self.valid_from is None or self.valid_from <= timezone.now().date())
        ):
            rule.matches.append(Match("mac", "--mac-source %s" % self.mac_address))
            rulev4 = copy.deepcopy(rule)
            if self.ipv4_address:
                rulev4.source = self.ipv4_address.ip
            return ([rulev4], [rule])
        else:
            return ([], [])
    iptables_rules_mangle_extra = ([Rule(chain="MAC_IP", in_interface="tun+", jump=Target("RETURN")), Rule(chain="MAC_IP", in_interface="tap+", jump=Target("RETURN"))], [Rule(chain="MAC_IP", jump=Target("MARK", '--set-mark %s' % config.ipt_mark['mac_ip']))])
    def iptables_rules_filter(self):
        return ([], [])
        rule=Rule()
        rule.chain = "MAC_IP"
        if self.vlan.interface:
            rule.in_interface = self.vlan.interface.name
        rule.jump = Target("RETURN")
        if (
            self.mac_address and self.active and
            (self.valid_until is None or self.valid_until >= timezone.now().date()) and
            (self.valid_from is None or self.valid_from <= timezone.now().date())
        ):
            rule.matches.append(Match("mac", "--mac-source %s" % self.mac_address))
            rulev4 = copy.deepcopy(rule)
            if self.ipv4_address:
                rulev4.source = self.ipv4_address.ip
            return ([rulev4], [rule])
        else:
            return ([], [])
        return ([], [])
    iptables_rules_filter_extra = ([], [Rule(chain="MAC_IP", jump=Target("REJECT"))])
    def iptables_rules_nat(self):
        return ([], [])

@produce_firewall
class MachineFirewall(BufferMessages):
    class Meta:
        verbose_name = "règle du pare-feu"
        verbose_name_plural = "règles du pare-feu"
        unique_together = (
          ("machine", "incoming", "protocol", "allow", "vlan", "ip_protocol"),
        )
        ordering = ("incoming", "priority", "-machine",)
    priority = models.IntegerField(default=100)
    machine = models.ForeignKey(Machine, blank=True, null=True, related_name='firewall_rules', verbose_name="machine")
    vlan = models.ForeignKey(Vlan, related_name='firewall_rules', blank=True, null=True)
    incoming = models.BooleanField(default=True, verbose_name="entrant")
    ip_protocol = models.CharField(max_length=4, choices=[('ipv4', 'ipv4'), ('ipv6', 'ipv6'), ('both', 'both')], verbose_name="protocole ip", default="ipv6")
    allow = models.BooleanField(default=True)
    protocol = models.CharField(max_length=4, blank=True, null=True, choices=[('udp', 'udp'), ('tcp', 'tcp'), ('icmp', 'icmp')], verbose_name="protocole")
    ports = models.CharField(max_length=255, blank=True, null=True, help_text="Une liste de port de la forme 22,80,110,25,6000:6010.")

    @property
    def outgoing(self):
        return not self.incoming
    @outgoing.setter
    def outgoing(self, value):
        self.incoming = not value
    @property
    def ipv4(self):
        return self.ip_protocol in ['ipv4', 'both']
    @property
    def ipv6(self):
        return self.ip_protocol in ['ipv6', 'both']
    @ipv4.setter
    def ipv4(self, value):
        if value:
            if self.ipv6:
                self.ip_protocol="both"
            else:
                self.ip_protocol="ipv4"
        else:
            self.ip_protocol="ipv6"
    @ipv6.setter
    def ipv6(self, value):
        if value:
            if self.ipv4:
                self.ip_protocol="both"
            else:
                self.ip_protocol="ipv6"
        else:
            self.ip_protocol="ipv4"
    @property
    def deny(self):
        return not self.allow
    @deny.setter
    def deny(self, value):
        self.allow = not value

    def __unicode__(self):
        if self.machine:
            name = self.machine
        elif self.vlan:
            name = "vlan %s" % self.vlan
        else:
            name = "All"
        if self.protocol:
            if self.ports:
                return "%s: port %s ouvert en %s" % (name, self.ports, self.protocol)
            else:
                return "%s: tout ports ouvert en %s" % (name, self.protocol)
        else:
            return "%s: accessible pour tout protocol de transport" % name

    def iptables_rules_nat(self):
        return ([], [])
    def iptables_rules_filter(self):
        rulesv4=[Rule()] if self.ipv4 else []
        rulesv6=[Rule()] if self.ipv6 else []
        for rule in rulesv4 + rulesv6:
            rule.chain = 'MACHINES'
            if self.incoming:
                #rule.in_interface = Interface.objects.get(uplink=True).name
                if self.vlan and self.vlan.interface:
                    rule.out_interface = self.vlan.interface.name
                else:
                    rule.in_interface = Interface.objects.get(uplink=True).name
            else:
                #rule.out_interface = Interface.objects.get(uplink=True).name
                if self.vlan and self.vlan.interface:
                    rule.in_interface = self.vlan.interface.name
                rule.out_interface = Interface.objects.get(uplink=True).name
                if self.machine and self.machine.mac_address:
                    rule.matches.append(Match("mac", "--mac-source %s" % self.machine.mac_address))
            if self.allow:
                rule.jump = Target('ACCEPT')
            else:
                rule.jump = Target('REJECT')
        if self.protocol:
            for rule in rulesv4:
                if self.protocol in ['tcp', 'udp', 'icmp']:
                    rule.protocol = self.protocol
                else:
                    raise ValueError("protocol inconnu %r" % self.protocol)
            for rule in rulesv6:
                if self.protocol in ['tcp', 'udp']:
                    rule.protocol = self.protocol
                elif self.protocol == 'icmp':
                    rule.protocol = 'ipv6-icmp'
                else:
                    raise ValueError("protocol inconnu %r" % self.protocol)


            # doit être en dernier du paramètre commun v4 et v6 et avant les paramètre différent v4, v6
            if self.protocol in ['tcp', 'udp'] and self.ports:
                for rule in rulesv4 + rulesv6:
                    ports = self.ports.split(",")
                    i=1
                    crule = copy.deepcopy(rule)
                    while ports[i*15:(i+1)*15]:
                        crule = copy.deepcopy(crule)
                        crule.matches.append(Match("multiport", '--dport %s' % ",".join(port_name_to_int(ports[i*15:(i+1)*15]))))
                        if rule in rulesv4:
                            rulesv4.append(copy.deepcopy(crule))
                        if rule in rulesv6:
                            rulesv6.append(copy.deepcopy(crule))
                        i+=1
                    rule.matches.append(Match("multiport", '--dport %s' % ",".join(port_name_to_int(ports[0:15]))))

        if self.machine:
            if self.incoming:
                for rule in rulesv4:
                    rule.destination = self.machine.ipv4_address.ip
                for rule in rulesv6:
                    rule.destination = self.machine.ipv6_address()
        return (rulesv4, rulesv6)

    def iptables_rules_mangle(self):
        return ([],[])
    def iptables_rules_raw(self):
        return ([],[])

    def save(self, *args, **kwargs):
        if not self.protocol in ["tcp", "udp"]:
            self.ports = None
        if self.machine:
            self.vlan = self.machine.vlan
        ret = super(MachineFirewall, self).save(*args, **kwargs)
        iptables_restore(self.__class__)
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        return ret

    def delete(self, *args, **kwargs):
        ret = super(MachineFirewall, self).delete(*args, **kwargs)
        iptables_restore(self.__class__)
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        return ret

@produce_firewall
class PortRedirection(BufferMessages):
    class Meta:
        verbose_name = "redirection de port en ipv4"
        verbose_name_plural = "redirections de port en ipv4"
        unique_together = (
          ("public_machine", "public_port", "protocol"),
        )
    public_machine = models.ForeignKey(Machine, limit_choices_to=Q(has_public_ipv4_address=True), related_name='+', verbose_name="machine publique")
    private_machine = models.ForeignKey(Machine, limit_choices_to=Q(has_public_ipv4_address=False), related_name='+', verbose_name="machine privée")
    # On peut mettre les port à None pour les protocol où avoir un port n'a pas de sens (genre le protocol 41)
    public_port = fields.IntegerRangeField(min_value=1, max_value=65535, blank=True, null=True, verbose_name="port publique")
    private_port = fields.IntegerRangeField(min_value=1, max_value=65535, blank=True, null=True, verbose_name="port privé") 
    protocol = models.CharField(max_length=3, choices=[('udp', 'udp'), ('tcp', 'tcp')], verbose_name="protocole")

    def __unicode__(self):
        return u"%s:%s -> %s:%s (%s)" % (self.public_machine, self.public_port, self.private_machine, self.private_port, self.protocol)

    def iptables_rules_nat(self):
        if self.private_machine.active and self.public_machine.ipv4_address:
            return ([Rule(
         chain='NAT_PORT_REDIRECT',
         protocol=self.protocol,
         destination=self.public_machine.ipv4_address.ip, 
         matches=[Match(self.protocol, '--dport %s' % port_name_to_int(self.public_port))],
         jump=Target('DNAT', '--to-destination %s:%s' % (self.private_machine.ipv4_address, self.private_port))
        )],[])
        else:
            return ([], [])
    def iptables_rules_filter(self):
        if self.private_machine.active:
            return ([Rule(
         chain='NAT_PORT_REDIRECT',
         protocol=self.protocol,
         destination=self.private_machine.ipv4_address.ip,
         matches=[Match(self.protocol, '--dport %s' % port_name_to_int(self.private_port))],
         jump=Target('ACCEPT'),
        )],[])
        else:
            return ([], [])
    def iptables_rules_mangle(self):
        if self.private_machine.active and self.public_machine.ipv4_address:
            return ([Rule(
         chain='NAT_PORT_REDIRECT',
         protocol=self.protocol,
         destination=self.public_machine.ipv4_address.ip,
         matches=[Match(self.protocol, '--dport %s' % port_name_to_int(self.public_port))],
         jump=Target('MARK', '--set-mark %s' % config.ipt_mark['masquerade']),
        )],[])
        else:
            return ([], [])
    def iptables_rules_raw(self):
        return ([],[])

    def save(self, *args, **kwargs):
        ret = super(PortRedirection, self).save(*args, **kwargs)
        iptables_restore(self.__class__)
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        return ret

    def delete(self, *args, **kwargs):
        ret = super(PortRedirection, self).delete(*args, **kwargs)
        iptables_restore(self.__class__)
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        return ret

@produce_firewall
class PortRangeRedirection(BufferMessages):
    class Meta:
        verbose_name = "redirection de plage de ports en ipv4"
        verbose_name_plural = "redirections de plages de ports en ipv4"
        unique_together = (
          ("public_machine", "port_range_start", "port_range_end", "protocol"),
        )
    public_machine = models.ForeignKey(Machine, limit_choices_to=Q(has_public_ipv4_address=True), related_name='+', verbose_name="machine publique")
    private_machine = models.ForeignKey(Machine, limit_choices_to=Q(has_public_ipv4_address=False), related_name='+', verbose_name="machine privée")
    # On peut mettre les port à None pour les protocol où avoir un port n'a pas de sens (genre le protocol 41)
    port_range_start = fields.IntegerRangeField(min_value=1, max_value=65535, blank=True, null=True, verbose_name="premier port")
    port_range_end = fields.IntegerRangeField(min_value=1, max_value=65535, blank=True, null=True, verbose_name="dernier port") 
    protocol = models.CharField(max_length=3, choices=[('udp', 'udp'), ('tcp', 'tcp')], verbose_name="protocole")

    def __unicode__(self):
        return u"%s:%s-%s -> %s: (%s)" % (self.public_machine, self.port_range_start, self.port_range_end, self.private_machine, self.protocol)

    def iptables_rules_nat(self):
        if self.private_machine.active and self.public_machine.ipv4_address:
            return ([Rule(
         chain='NAT_RANGE_PORT_REDIRECT',
         protocol=self.protocol,
         destination=self.public_machine.ipv4_address.ip, 
         matches=[Match(self.protocol, '--dport %s:%s' % (port_name_to_int(self.port_range_start), port_name_to_int(self.port_range_end)))],
         jump=Target('DNAT', '--to-destination %s' % (self.private_machine.ipv4_address,))
        )],[])
        else:
            return ([], [])
    def iptables_rules_filter(self):
        if self.private_machine.active:
            return ([Rule(
         chain='NAT_RANGE_PORT_REDIRECT',
         protocol=self.protocol,
         destination=self.private_machine.ipv4_address.ip,
         matches=[Match(self.protocol, '--dport %s:%s' % (port_name_to_int(self.port_range_start), port_name_to_int(self.port_range_end)))],
         jump=Target('ACCEPT'),
        )],[])
        else:
            return ([], [])
    def iptables_rules_mangle(self):
        if self.private_machine.active and self.public_machine.ipv4_address:
            return ([Rule(
         chain='NAT_RANGE_PORT_REDIRECT',
         protocol=self.protocol,
         destination=self.public_machine.ipv4_address.ip,
         matches=[Match(self.protocol, '--dport %s:%s' % (port_name_to_int(self.port_range_start), port_name_to_int(self.port_range_end)))],
         jump=Target('MARK', '--set-mark %s' % config.ipt_mark['masquerade']),
        )],[])
        else:
            return ([], [])
    def iptables_rules_raw(self):
        return ([],[])

    def save(self, *args, **kwargs):
        ret = super(PortRangeRedirection, self).save(*args, **kwargs)
        iptables_restore(self.__class__)
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        return ret

    def delete(self, *args, **kwargs):
        ret = super(PortRangeRedirection, self).delete(*args, **kwargs)
        iptables_restore(self.__class__)
        self.add_message(messages.SUCCESS, "Pare-feu mis à jour avec succès.")
        return ret

class Filtrage(models.Model):
    class Meta:
        abstract = True
        managed = False

    @staticmethod
    def _last_where(machines):
        where=Q(pk=-1)
        try:
            iter(machines)
        except TypeError:
            machines = [machines]
        for machine in machines:
            if machine.mac_address:
                where = where | Q(mac=machine.mac_address)
        return where

    @classmethod
    @cache(3600*24*365*10)
    def month(cls, machines, year, month):
        day = datetime.date(year, month, 1)
        now = timezone.now().date()
        if day >= datetime.date(now.year, now.month, 1):
            raise ValueError("`month` is not in the past")
        where = cls._last_where(machines)
        month = timezone.make_aware(datetime.datetime(year, month, 1), pytz.timezone(settings.TIME_ZONE))
        if month.month < 12:
            month_n = timezone.make_aware(datetime.datetime(year, month.month + 1, 1), pytz.timezone(settings.TIME_ZONE))
        else:
            month_n = timezone.make_aware(datetime.datetime(year + 1, 1, 1), pytz.timezone(settings.TIME_ZONE))
        bytes = cls.objects.filter(Q(stamp_inserted__gt=month) & Q(stamp_inserted__lt=month_n) & (where)).aggregate(Sum('bytes'))['bytes__sum']
        if bytes:
            return bytes
        else:
            return 0

    @classmethod
    @cache(3600*24*365*10)
    def year(cls, machines, year):
        day = datetime.date(year, 1, 1)
        if year >= timezone.now().date().year:
            raise ValueError("`year` is not in the past")
        where = cls._last_where(machines)
        day = timezone.make_aware(datetime.datetime(day.year, day.month, day.day), pytz.timezone(settings.TIME_ZONE))
        day_n = timezone.make_aware(datetime.datetime(day.year + 1, day.month, day.day), pytz.timezone(settings.TIME_ZONE))
        bytes = cls.objects.filter(Q(stamp_inserted__gt=day) & Q(stamp_inserted__lt=day_n) & (where)).aggregate(Sum('bytes'))['bytes__sum']
        if bytes:
            return bytes
        else:
            return 0

    @classmethod
    @cache(3600*24*365*10)
    def day(cls, day):
        if not isinstance(day, datetime.date):
            raise ValueError("`day` should be of type datetime.date")
        if year >= timezone.now().date().year:
            raise ValueError("`year` is not in the past")
        where = cls._last_where(machines)
        day = timezone.make_aware(datetime.datetime(day.year, day.month, day.day), pytz.timezone(settings.TIME_ZONE))
        bytes = cls.objects.filter(Q(stamp_inserted__gt=day) & Q(stamp_inserted__lt=day + datetime.timedelta(days=1)) & (where)).aggregate(Sum('bytes'))['bytes__sum']
        if bytes:
            return bytes
        else:
            return 0

    @classmethod
    @cache(600)
    def last_day(cls, machines):
        where = cls._last_where(machines)
        bytes =  cls.objects.filter(Q(stamp_inserted__gt=timezone.now() - datetime.timedelta(days=1)) & (where)).aggregate(Sum('bytes'))['bytes__sum']
        if bytes:
            return bytes
        else:
            return 0
    @classmethod
    @cache(3600)
    def last_week(cls, machines):
        where = cls._last_where(machines)
        bytes = cls.objects.filter(Q(stamp_inserted__gt=timezone.now() - datetime.timedelta(weeks=1)) & (where)).aggregate(Sum('bytes'))['bytes__sum']
        if bytes:
            return bytes
        else:
            return 0
    @classmethod
    @cache(86400)
    def last_month(cls, machines):
        where = cls._last_where(machines)
        bytes = cls.objects.filter(Q(stamp_inserted__gt=timezone.now() - datetime.timedelta(days=30)) & (where)).aggregate(Sum('bytes'))['bytes__sum']
        if bytes:
            return bytes
        else:
            return 0
    @classmethod
    @cache(86400)
    def last_year(cls, machines):
        where = cls._last_where(machines)
        bytes = cls.objects.filter(Q(stamp_inserted__gt=timezone.now() - datetime.timedelta(weeks=52)) & (where)).aggregate(Sum('bytes'))['bytes__sum']
        if bytes:
            return bytes
        else:
            return 0
    @classmethod
    @cache()
    def period(cls, machines, from_, to_):
        where = cls._last_where(machines)
        bytes = cls.objects.filter(Q(stamp_inserted__gt=from_) & Q(stamp_updated__lt=to_) & Q(where)).aggregate(Sum('bytes'))['bytes__sum']
        if bytes:
            return bytes
        else:
            return 0

    @classmethod
    @cache()
    def ipv6_addresses(cls, machine):
        fields = dict((f.name, f) for f in cls._meta.fields)
        cursor = connection.cursor()
        cursor.execute("SELECT %(ip)s, MIN(stamp_inserted) AS stamp_inserted, MAX(stamp_updated) AS stamp_updated  FROM filtrage.%(table)s WHERE %(mac)s=%%s AND %(ip)s LIKE '%%%%:%%%%' AND %(ip)s NOT LIKE 'fe80:%%%%' GROUP BY %(ip)s" % {
        'ip':fields["ip"].db_column, 'mac':fields["mac"].db_column, 'table':cls._meta.db_table},
        [machine.mac_address])
        return dictfetchall(cursor)

    def save(self, *args, **kwargs):
        pass

class Upload(Filtrage):
    class Meta:
       managed = False
       db_table = 'upload'
       unique_together = ("mac", "vlan", "ip", "ip_proto", "stamp_inserted")

    mac = models.CharField(max_length=17, db_column="mac_src")
    vlan = models.IntegerField()
    ip = models.CharField(max_length=49, db_column="ip_src")
    ip_proto = models.CharField(max_length=10)
    packets = models.IntegerField()
    bytes = models.BigIntegerField()
    stamp_inserted = models.DateTimeField()
    stamp_updated = models.DateTimeField()

    def __unicode__(self):
        return u"Upload"
    def __str__(self):
        return "Upload"

class Download(Filtrage):
    class Meta:
       managed = False
       db_table = 'download'
       unique_together = ("mac", "vlan", "ip", "ip_proto", "stamp_inserted")

    mac = models.CharField(max_length=17, db_column="mac_dst")
    vlan = models.IntegerField()
    ip = models.CharField(max_length=49, db_column="ip_dst")
    ip_proto = models.CharField(max_length=10)
    packets = models.IntegerField()
    bytes = models.BigIntegerField()
    stamp_inserted = models.DateTimeField()
    stamp_updated = models.DateTimeField()

    def __unicode__(self):
        return u"Download"
    def __str__(self):
        return "Download"
