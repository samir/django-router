# ⁻*- coding: utf-8 -*-
from django.contrib import messages
from django.core.cache import cache as _djcache

import sys
import random
import string
import netifaces
import netaddr
import config
import collections
import copy
import socket
from hashlib import sha1
from subprocess import Popen, PIPE

from models import iptables_classes

def cache(seconds = 900):
    """
        Cache the result of a function call for the specified number of seconds, 
        using Django's caching mechanism.
        Assumes that the function never returns None (as the cache returns None to indicate a miss), and that the function's result only depends on its parameters.
        Note that the ordering of parameters is important. e.g. myFunction(x = 1, y = 2), myFunction(y = 2, x = 1), and myFunction(1,2) will each be cached separately. 

        Usage:

        @cache(600)
        def myExpensiveMethod(parm1, parm2, parm3):
            ....
            return expensiveResult
`
    """
    def serialize(x):
        if isinstance(x, str):
            return x
        elif isinstance(x, unicode):
            return str(x)
        elif isinstance(x, dict):
            x = x.items()
            return serialize(x)
        try:
            l=[serialize(i) for i in x]
            l.sort() 
            return ".".join(l)
        except TypeError:
            return str(x)

    def doCache(f):
        def x(*args, **kwargs):
                if 'refresh_cache' in kwargs:
                    refresh = kwargs['refresh_cache']
                    del kwargs['refresh_cache']
                else:
                    refresh = False
                key = str(f.__module__) + str(f.__name__) + serialize(args) +  serialize(kwargs)
                key = sha1(key).hexdigest()
                if not refresh:
                    result = _djcache.get(key)
                else:
                    result = None
                if result is None:
                    result = f(*args, **kwargs)
                    _djcache.set(key, result, seconds)
                return result
        return x
    return doCache

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


def format_bytes(bytes):
    if bytes is None:
        return "0 o"
    if bytes < 2048:
        return "%s o" % bytes
    else:
        bytes = round(bytes / 1024.0, 2)
    if bytes < 2048:
        return "%s Ko" % bytes
    else:
        bytes = round(bytes / 1024.0, 2)
    if bytes < 2048:
        return "%s Mo" % bytes
    else:
        bytes = round(bytes / 1024.0, 2)
    if bytes < 2048:
        return "%s Go" % bytes
    else:
        bytes = round(bytes / 1024.0, 2)
    return "%s To" % bytes

def has(l1, l2):
    for i in l1:
        if i in l2:
            return True
    return False

def net_to_iface(net):
    net = netaddr.IPNetwork(net)
    ifaces=[]
    if net.version == 4:
        INET_TYPE = netifaces.AF_INET
    elif net.version == 6:
        INET_TYPE = netifaces.AF_INET6
    else:
        raise ValueError("ipv%s ?" % net.version)
    for iface in netifaces.interfaces():
        try:
            for params in netifaces.ifaddresses(iface)[INET_TYPE]:
                pnet="%s/%s" % (params['addr'], params['netmask'])
                if netaddr.IPNetwork(pnet) == net:
                    ifaces.append(iface)
        except KeyError:
             pass
    return ifaces

def format_mac(mac):
    """ Formatage des adresses mac
    Transforme une adresse pour obtenir la forme xx:xx:xx:xx:xx:xx
    Retourne la mac formatée.
    """
    mac = unicode(mac).lower()
    mac = netaddr.EUI(mac)
    return unicode(str(mac).replace('-', ':')).lower()

def random_wifi_password():
    return u''.join( random.choice(filter(lambda x: x != 'l' and x != 'o', string.lowercase) + filter(lambda x: x != '1' and x != '0', string.digits)) for i in range(10))

def run_once(f):
    if hasattr(f, "has_run"):
        f.has_run = False
        return f
    def wrapper(*args, **kwargs):
        if not wrapper.has_run:
            wrapper.has_run = True
            return f(*args, **kwargs)
    wrapper.has_run = False
    return wrapper


def iptables(cmd, ipv6=False):
    params = ['sudo']
    if ipv6:
        params.append('/sbin/ip6tables')
    else:
        params.append('/sbin/iptables')
    params.extend(cmd)
    p = Popen(params , stdin=PIPE, stdout=PIPE, stderr=PIPE)
    (stdout, stderr) = p.communicate()
    if stderr:
        sys.stderr.write("%s\n%s" % (' '.join(cmd), stderr))
        raise ValueError(stderr, params)


def iptables_restore(classes=iptables_classes, nono=False):
    if not isinstance(classes, list):
        classes = [classes]
    (ipv4, ipv6) = iptables_restore_str(classes)
    if nono:
        print(ipv4)
        print("")
        print(ipv6)
        return
    params4 = ['sudo', '/sbin/iptables-restore', '--noflush']
    params6 = ['sudo', '/sbin/ip6tables-restore', '--noflush']
    if ipv4:
        p = Popen(params4 , stdin=PIPE, stdout=PIPE, stderr=PIPE)
        (stdout, stderr) = p.communicate(input=ipv4)
        if stderr:
            sys.stderr.write(stderr)
            sys.stdout.write(ipv4)
            raise ValueError(stderr)
    if ipv6:
        p = Popen(params6 , stdin=PIPE, stdout=PIPE, stderr=PIPE)
        (stdout, stderr) = p.communicate(input=ipv6)
        if stderr:
            sys.stderr.write(stderr)
            sys.stdout.write(ipv6)
            raise ValueError(stderr)
def iptables_restore_str(classes):

    def format(dico):
        str=""
        for table in dico:
            if not [chain for chain in dico[table] if chain not in config.iptables_default_chains]:
                continue
            str += '*%s\n' % table
            for chain in config.iptables_default_chains:
                if chain in dico[table]:
                    str += ':%s %s [0:0]\n' % (chain, chain in config.iptables_default_chains and 'ACCEPT' or '-')
            for chain in dico[table]:
                if not chain in config.iptables_default_chains:
                    str += ':%s %s [0:0]\n' % (chain, chain in config.iptables_default_chains and 'ACCEPT' or '-')
            for chain in dico[table]:
                for rule in dico[table][chain]:
                    str += '-A %s %s\n' % (chain, ' '.join(rule.specbits()))
            str += 'COMMIT\n'
        return str.replace("--log-prefix LOG_ALL", '--log-prefix "LOG_ALL "')

    ipv4=collections.defaultdict(lambda:collections.defaultdict(list))
    ipv6=collections.defaultdict(lambda:collections.defaultdict(list))
    default_rules_list = {
            'raw': { 'OUTPUT':[], 'PREROUTING':[] },
            'mangle':{'INPUT':[], 'OUTPUT':[], 'FORWARD':[], 'PREROUTING':[], 'POSTROUTING':[]},
            'filter':{'INPUT':[], 'OUTPUT':[], 'FORWARD':[]},
            'nat':{'OUTPUT':[], 'PREROUTING':[], 'POSTROUTING':[]}
        }
    
    ipv4.update(copy.deepcopy(default_rules_list))
    ipv6.update(copy.deepcopy(default_rules_list))
    def aux(table, rules, dico):
      if rules:
          for rule in rules:
              dico[table].setdefault(rule.chain, []).append(rule)
    for classe in classes:
        for table in config.ipables_tables:
            extra = getattr(classe, 'iptables_rules_%s_extra' % table, ([],[]))[0]
            aux(table, extra, ipv4)
            aux(table, extra, ipv6)
            for obj in classe.objects.all():
                (rulesv4, rulesv6) = getattr(obj, 'iptables_rules_%s' % table)()
                aux(table, rulesv4, ipv4)
                aux(table, rulesv6, ipv6)
            extra = getattr(classe, 'iptables_rules_%s_extra' % table, ([],[]))[1]
            aux(table, extra, ipv4)
            aux(table, extra, ipv6)
          
    return (format(ipv4), format(ipv6))



def port_name_to_int(l):
    if isinstance(l, (int, long)):
        return str(l)
    elif isinstance(l, str) or isinstance(l, unicode):
        try:
            int(l)
            return l
        except ValueError:
            try:
                return str(socket.getservbyname(l))
            except:
                print("%r" % l)
                raise
    else:
        return [port_name_to_int(p) for p in l]
