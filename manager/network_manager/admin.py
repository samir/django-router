# ⁻*- coding: utf-8 -*-


from django.conf import settings
from django.utils import timezone
from django.utils.html import format_html
from django.contrib import admin
from django.contrib import messages
from django.contrib.auth.models import Permission

from guardian.admin import GuardedModelAdmin

import models
import forms
import utils
import datetime

class ModelAdmin(GuardedModelAdmin):

    response_delete_messages = []
    def response_change(self, request, obj):
        obj.enqueue_messages(request)
        return super(ModelAdmin, self).response_change(request, obj)
    def response_add(self, request, obj, post_url_continue=None):
        obj.enqueue_messages(request)
        return super(ModelAdmin, self).response_add(request, obj, post_url_continue=None)
    def response_delete(self, request, obj_display):
        for msg_type, msg_data in self.response_delete_messages:
            messages.add_message(request, msg_type, msg_data)
        return super(ModelAdmin, self).response_delete(request, obj_display)


class MultiMachineAdmin(ModelAdmin):
    class Media:
        css = {
             'all': ('css/admin/machine.css',)
        }
    form = forms.MultiMachineForm
    list_display = ('active', "name", 'wifi', 'fixe', 'last_connection', 'valid_from', 'valid_until', 'date_added', 'owner')
    list_display_links = ("name", )
    fields = ("name", 'wifi', 'fixe', 'active', 'vlan', 'valid_from', 'valid_until',
      'mac_addresses', 'last_connection', 'date_added', 'owner',
    )
    readonly_fields=('wifi_password_func', 'last_connection', 'date_added', 'last_modified', 'date_added',
    'mac_addresses')
    actions=("delete_selected", "enable", "disable")
    list_filter=('active', 'last_connection', 'wifi', 'fixe', 'owner')

    def wifi_password_func(self, obj):
        if obj.wifi:
            return obj.wifi_password
        else:
            return "N/A"
    wifi_password_func.short_description = "Mot de passe WiFi"

    def mac_addresses(self, obj):
        macs=list(obj.macaddressfixe_set.all()) + list(obj.macaddresswifi_set.all())
        html='<ul>'
        for mac in macs:
            html+='<li>%s</li>' % mac.mac_address
        html+='</ul>'
        return html
    mac_addresses.allow_tags = True
    mac_addresses.short_description = "Liste des addresses mac utilisées"

    def get_fields(self, request, obj=None):
        fields = super(MultiMachineAdmin, self).get_fields(request, obj)
        if obj and obj.wifi:
            fields = fields[0:2] + ('wifi_password_func',) + fields[2:]
        return fields

    def disable(self, request, queryset):
        queryset.update(active=False)
    disable.short_description = "Désactiver les machines sélectionnées"
    def enable(self, request, queryset):
        queryset.update(active=True)
    enable.short_description = "Activer les machines sélectionnées"
    def delete_selected(self, request, queryset):
        queryset.delete()
        utils.iptables_restore()
    delete_selected.short_description = "Supprimer les éléments sélectionnés"

    def get_form(self, request, obj=None, **kwargs):
        AdminForm = super(MultiMachineAdmin, self).get_form(request, obj, **kwargs)
        class ModelFormMetaClass(AdminForm):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return AdminForm(*args, **kwargs)

        return ModelFormMetaClass


class MachineAdmin(ModelAdmin):
    class Media:
        css = {
             'all': ('css/admin/machine.css',)
        }
    form = forms.MachineForm
    list_display = ('active', "name", 'wifi', 'mac_address', 'last_connection', 'valid_from', 'valid_until', 'date_added', 'ipv4_address', 'owner')
    list_display_links = ("name", )
    #list_editable = ('valid_until',)
    fields = ("name", 'wifi', 'active', 'vlan', 'ipv4_address', 'canonical_ipv6_address', 'domain_name', 'valid_from', 'valid_until',
      'mac_address', 'manufacturer', 'last_connection', 'date_added', 'owner', "recent_ipv6_addresses", "all_ipv6_addresses", "traffic_last_day",
      "traffic_last_week", "traffic_last_month", "traffic_last_year")#, 'last_modified', 'date_added')
    readonly_fields=('wifi_password_func', 'canonical_ipv6_address', 'domain_name', 'manufacturer', 'last_connection', 'date_added', 'last_modified', 'date_added',
      "recent_ipv6_addresses", "all_ipv6_addresses", "traffic_last_day", "traffic_last_week", "traffic_last_month", "traffic_last_year")
    actions=("delete_selected", "enable", "disable")
    list_filter=('active', 'last_connection', 'wifi', 'owner')

    def wifi_password_func(self, obj):
        if obj.wifi:
            return obj.wifi_password
        else:
            return "N/A"
    wifi_password_func.short_description = "Mot de passe WiFi"

    def get_fields(self, request, obj=None):
        fields = super(MachineAdmin, self).get_fields(request, obj)
        if obj and obj.wifi:
            fields = fields[0:2] + ('wifi_password_func',) + fields[2:]
        return fields

    def domain_name(self, obj):
        return obj.dns_name(fqdn=True)
    domain_name.short_description = u"Nom de domaine"

    def canonical_ipv6_address(self, obj):
        return obj.ipv6_address()
    canonical_ipv6_address.short_description = u"Adresse ipv6"

    def recent_ipv6_addresses(self, obj):
        ipv6=obj.ipv6_addresses()
        return ", ".join(ip['ip_dst'] for ip in ipv6 if ip['stamp_updated']>= (datetime.datetime.now() - datetime.timedelta(hours=6)))
    recent_ipv6_addresses.short_description = u"Adresses ipv6 récemment utilisées"

    def all_ipv6_addresses(self, obj):
        ipv6=obj.ipv6_addresses()
        ipv6.sort(lambda x,y:-1 if x['stamp_updated'] < y['stamp_updated'] else (0 if x['stamp_updated'] == y['stamp_updated'] else 1))
        html='<ul>'
        for ip in ipv6:
            html+='<li>%s</li>' % ip['ip_dst']
        html+='</ul>'
        return html
    all_ipv6_addresses.allow_tags = True
    all_ipv6_addresses.short_description = "Historique des ipv6"

    def traffic_last_day(self, obj):
        up = utils.format_bytes(models.Upload.last_day(obj))
        down = utils.format_bytes(models.Download.last_day(obj))
        return "UP: %s | DL: %s" % (up, down)
    traffic_last_day.short_description = "Traffic sur un jour"

    def traffic_last_week(self, obj):
        up = utils.format_bytes(models.Upload.last_week(obj))
        down = utils.format_bytes(models.Download.last_week(obj))
        return "UP: %s | DL: %s" % (up, down)
    traffic_last_week.short_description = "Traffic sur une semaine"

    def traffic_last_month(self, obj):
        up = utils.format_bytes(models.Upload.last_month(obj))
        down = utils.format_bytes(models.Download.last_month(obj))
        return "UP: %s | DL: %s" % (up, down)
    traffic_last_month.short_description = "Traffic sur un mois"

    def traffic_last_year(self, obj):
        up = utils.format_bytes(models.Upload.last_year(obj))
        down = utils.format_bytes(models.Download.last_year(obj))
        return "UP: %s | DL: %s" % (up, down)
    traffic_last_year.short_description = "Traffic sur un an"

    def disable(self, request, queryset):
        queryset.update(active=False)
    disable.short_description = "Désactiver les machines sélectionnées"
    def enable(self, request, queryset):
        queryset.update(active=True)
    enable.short_description = "Activer les machines sélectionnées"
    def delete_selected(self, request, queryset):
        queryset.delete()
        utils.iptables_restore()
    delete_selected.short_description = "Supprimer les éléments sélectionnés"

    def get_form(self, request, obj=None, **kwargs):

        AdminForm = super(MachineAdmin, self).get_form(request, obj, **kwargs)
        class ModelFormMetaClass(AdminForm):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return AdminForm(*args, **kwargs)

        return ModelFormMetaClass

    response_delete_messages = [(messages.SUCCESS, "Pare-feu mis à jour avec succès."), (messages.SUCCESS, "Configuration dhcp mise à jour")]

class PortRedirectionAdmin(ModelAdmin):
    #list_display = ('public_machine', 'private_machine', 'protocol', 'public_port', 'private_port')
    list_display = ('private_machine', 'protocol', 'public_port', 'private_port', 'public_machine')
    list_display_links = list_display
    actions=("delete_selected", )

    def delete_selected(self, request, queryset):
        queryset.delete()
        utils.iptables_restore(models.PortRedirection)

    delete_selected.short_description = "Supprimer les éléments sélectionnés"

    response_delete_messages = [(messages.SUCCESS, "Pare-feu mis à jour avec succès.")]

class PortRangeRedirection(ModelAdmin):
    list_display = ('private_machine', 'protocol', 'port_range_start', 'port_range_end', 'public_machine')
    list_display_links = list_display
    actions=("delete_selected", )

    def delete_selected(self, request, queryset):
        queryset.delete()
        utils.iptables_restore(models.PortRedirection)

    delete_selected.short_description = "Supprimer les éléments sélectionnés"

    response_delete_messages = [(messages.SUCCESS, "Pare-feu mis à jour avec succès.")]

class MachineFirewallAdmin(ModelAdmin):
    class Media:
        css = {
             'all': ('css/admin/machine.css',)
        }

    form = forms.MachineFirewallForm
    list_display = ("incoming", 'priority', 'machines', 'allow', 'ip', 'protocoles', 'ports2')
    list_display_links = list_display
    actions=("delete_selected", )

    def delete_selected(self, request, queryset):
        queryset.delete()
        utils.iptables_restore(models.MachineFirewall)

    delete_selected.short_description = "Supprimer les éléments sélectionnés"

    def machines(self, obj):
        if obj.machine:
            return "machine: %s" % obj.machine
        elif obj.vlan:
            return "vlan: %s" % obj.vlan
        else:
            return u"*"
    def ip(self, obj):
        if obj.ip_protocol == "both":
#            return None
            return "v4 et v6"
        elif obj.ip_protocol == "ipv4":
#            return False
            return "v4"
        elif obj.ip_protocol == "ipv6":
#            return True
            return "v6"
#    ip.boolean = True
#    ip.short_description = "
    def protocoles(self, obj):
        if obj.protocol:
            return obj.protocol
        else:
            return u"tous"
    def ports2(self, obj):
        if obj.ports:
            return obj.ports
        else:
            return u"tous"
    ports2.short_description = "ports"


    response_delete_messages = [(messages.SUCCESS, "Pare-feu mis à jour avec succès.")]

class Ipv4AddressAdmin(ModelAdmin):
    form = forms.Ipv4AddressForm
    list_display = ("ip", "vlan", "public")
    list_display_links = ("ip",)
    #list_editable = ('vlan', )
    fields = ("ip", "vlan")

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('ip', )
        return self.readonly_fields

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        #Disable delete
        actions = super(Ipv4AddressAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    response_delete_messages = [(messages.SUCCESS, "Pare-feu mis à jour avec succès.")]

class InterfaceAdmin(ModelAdmin):
    list_display = ("name", "uplink")

class VlanAdmin(ModelAdmin):
    class Media:
        css = {
             'all': ('css/admin/machine.css',)
        }
    list_display = ("id", "name", "network_ipv4", "network_ipv6", "interface")
    list_display_links = ("name",)

    response_delete_messages = [(messages.SUCCESS, "Pare-feu mis à jour avec succès.")]


class OwnerAdmin(ModelAdmin):
    list_display = ("name", "address", "valid_from", "valid_until", "airbnb")
    fields=("name", "address", "phone_number", "email", "valid_from", "valid_until", "airbnb", "machines", "traffic_last_day", "traffic_last_week", "traffic_last_month", "traffic_last_year")
    readonly_fields=("machines", "traffic_last_day", "traffic_last_week", "traffic_last_month", "traffic_last_year")
    list_filter=("airbnb",)
    form = forms.OwnerForm

    def machines(self, obj):
        html="<ul>"
        for machine in obj.machines.all():
            html+=format_html('<li><a href="{0}">{1}</a></li>', machine.get_admin_url(), machine)
        html+="</ul>"
        return html
    machines.short_description = "Machines"
    machines.allow_tags = True
    @utils.cache(900)
    def traffic_last_day(self, obj):
        up = utils.format_bytes(models.Upload.last_day(obj.machines.all()))
        down = utils.format_bytes(models.Download.last_day(obj.machines.all()))
        return "UP: %s | DL: %s" % (up, down)
    traffic_last_day.short_description = "Traffic sur un jour"
    @utils.cache(3600*6)
    def traffic_last_week(self, obj):
        up = utils.format_bytes(models.Upload.last_week(obj.machines.all()))
        down = utils.format_bytes(models.Download.last_week(obj.machines.all()))
        return "UP: %s | DL: %s" % (up, down)
    traffic_last_week.short_description = "Traffic sur une semaine"
    @utils.cache(86400)
    def traffic_last_month(self, obj):
        up = utils.format_bytes(models.Upload.last_month(obj.machines.all()))
        down = utils.format_bytes(models.Download.last_month(obj.machines.all()))
        return "UP: %s | DL: %s" % (up, down)
    traffic_last_month.short_description = "Traffic sur un mois"
    @utils.cache(86400*7)
    def traffic_last_year(self, obj):
        up = utils.format_bytes(models.Upload.last_year(obj.machines.all()))
        down = utils.format_bytes(models.Download.last_year(obj.machines.all()))
        return "UP: %s | DL: %s" % (up, down)
    traffic_last_year.short_description = "Traffic sur un an"

# Register your models here.
admin.site.register(models.Machine, MachineAdmin)
admin.site.register(models.Ipv4Address, Ipv4AddressAdmin)
admin.site.register(models.PortRedirection, PortRedirectionAdmin)
admin.site.register(models.PortRangeRedirection, PortRangeRedirection)
admin.site.register(models.Vlan, VlanAdmin)
admin.site.register(models.Interface, InterfaceAdmin)
admin.site.register(models.MachineFirewall, MachineFirewallAdmin)
admin.site.register(models.Owner, OwnerAdmin)
admin.site.register(models.MultiMachine, MultiMachineAdmin)
admin.site.register(Permission)
