# ⁻*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from network_manager.models import Machine, MultiMachine
from network_manager.utils import iptables_restore

class Command(BaseCommand):
    help = 'Désactive les machines expiré'

    def handle(self, *args, **options):
        machines = Machine.objects.filter(active=True, valid_until__lt=timezone.now().date())
        machines_multi = MultiMachine.objects.filter(active=True, valid_until__lt=timezone.now().date())
        machines.update(active=False)
        machines_multi.update(active=False)
        if len(machines) > 0 or len(machines_multi) > 0:
            print "Desactivation des machines :"
            for machine in machines:
                print "* %s" % machine.name
            for machine in machines_multi:
                print "* %s" % machine.name
            iptables_restore()
