# ⁻*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django import forms
from django.contrib import messages
from django.forms import ModelForm
from django.db.models import Q
from django.utils.safestring import mark_safe
from django.utils import timezone

import netaddr
import socket

import config
from models import Machine, Ipv4Address, Vlan, MachineFirewall, MultiMachine, Owner

from utils import format_mac

class HorizontalRadioRenderer(forms.RadioSelect.renderer):
  def render(self):
    return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))

class MachineFirewallForm(ModelForm):
    class Meta:
        model = MachineFirewall
        exclude = []

    incoming = forms.TypedChoiceField(choices=[(True, "entrant"), (False, "sortant")], widget=forms.RadioSelect(renderer=HorizontalRadioRenderer), label="sens", initial = True)
    ip_protocol = forms.TypedChoiceField(choices=[("ipv6", "ipv6"), ("ipv4", "ipv4"), ("both", "ipv4 et ipv6")], widget=forms.RadioSelect(renderer=HorizontalRadioRenderer), label="protocol ip", initial="ipv6")
    allow = forms.TypedChoiceField(choices=[(True, "autoriser"), (False, "refuser")], widget=forms.RadioSelect(renderer=HorizontalRadioRenderer), label="comportement", initial=True)

    def clean(self, *args, **kwargs):
        return super(MachineFirewallForm, self).clean(*args, **kwargs)

    def clean_ports(self, *args, **kwargs):
        ports = self.cleaned_data['ports']
        ports_list = []
        def valide_port(port):
            port=port.strip()
            try:
                port = int(port)
                if port<0 or port > 65535:
                    raise ValidationError(u"%s n'est pas un port valide, il doit être comprit entre 1 et 65535" % port)
                return str(port)
            except ValueError:
                try:
                    socket.getservbyname(port)
                    return port
                except socket.error:
                    raise ValidationError(u"Le port %s est inconnu ou mal formaté" % port)
        if ports:
            for port in ports.split(","):
                if ":" in port:
                    p1, p2 = port.split(":")
                    ports_list.append("%s:%s" % (valide_port(p1), valide_port(p2)))
                else:
                    ports_list.append(valide_port(port))
            return ",".join(ports_list)
        else:
            return None

#    def __init__(self, *args, **kwargs):
#        super(MachineFirewallForm, self).__init__(*args, **kwargs)
#        self.fields['protocol'].widget=forms.RadioSelect(renderer=HorizontalRadioRenderer)
class Ipv4AddressForm(ModelForm):
    class Meta:
        model = Ipv4Address
        exclude = []

    def clean(self, *args, **kwargs):
        ip = netaddr.IPAddress(self.cleaned_data.get('ip', self.instance.ip))
        if self.cleaned_data['vlan'] and (not self.cleaned_data['vlan'].network_ipv4 or not ip in netaddr.IPNetwork(self.cleaned_data['vlan'].network_ipv4)):
            raise ValidationError(u"L'adresse ip n'est pas dans le réseau %s du vlan %s" % (self.cleaned_data['vlan'].network_ipv4, self.cleaned_data['vlan'].id))
        if not self.cleaned_data['vlan']:
            vlans=[]
            for vlan in Vlan.objects.filter(~Q(network_ipv4=None)):
                if ip in netaddr.IPNetwork(vlan.network_ipv4):
                    vlans.append(vlan)
            if vlans:
                raise ValidationError(u"L'adresse ip correspond à un vlan, merci d'en affecter un : %s" % ', '.join("vlan %s" % vlan.id for vlan in vlans))
        return super(Ipv4AddressForm, self).clean(*args, **kwargs)


class MultiMachineForm(ModelForm):
    class Meta:
        model = MultiMachine
        exclude = []

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(MultiMachineForm, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        if self.cleaned_data['owner'] and self.cleaned_data['owner'].valid_until != self.cleaned_data['valid_until']:
            if not self.cleaned_data['owner'].valid_until:
                limit = "aucune"
            else:
                limit = self.cleaned_data['owner'].valid_until.strftime("%d/%m/%Y")
            messages.add_message(self.request, messages.WARNING, "Limite de validité fixée à celle du propriétaire : %s" % limit)
            self.cleaned_data['valid_until'] = self.cleaned_data['owner'].valid_until
        if self.cleaned_data['owner'] and self.cleaned_data['owner'].valid_from != self.cleaned_data['valid_from']:
            if not self.cleaned_data['owner'].valid_from:
                limit = "aucune"
            else:
                limit = self.cleaned_data['owner'].valid_from.strftime("%d/%m/%Y")
            messages.add_message(self.request, messages.WARNING, "Limite de début fixée à celle du propriétaire : %s" % limit)
            self.cleaned_data['valid_from'] = self.cleaned_data['owner'].valid_from
        if self.cleaned_data['fixe'] and self.cleaned_data['active']:
            id = self.instance.id if self.instance else None
            self.check_dates(self.cleaned_data['vlan'], self.cleaned_data['valid_from'], self.cleaned_data['valid_until'], id=id)
        return super(MultiMachineForm, self).clean(*args, **kwargs)

    def clean_name(self):
        if len(Machine.objects.filter(name=self.cleaned_data['name'])) > 0:
            raise forms.ValidationError("Nom de machine déjà utilisé")
        return self.cleaned_data['name']

    @staticmethod
    def check_dates(vlan, valid_from, valid_until, id=None):
            machines = MultiMachine.objects.filter(
                Q(vlan=vlan) &
                Q(active=True) & Q(fixe=True) & ~Q(id=id)
            )
            for machine in machines:
                if (
                    # +\   |         |    \+
                    (machine.valid_from is None and machine.valid_until is None) or
                    # +|   \         \    |+
                    (valid_from is None and valid_until is None) or
                    # +|\      \|
                    (machine.valid_from is None and valid_from is None) or
                    #  |\       \|+
                    (machine.valid_until is None and valid_until is None) or
                    # +\    |    \
                    (machine.valid_from is None and valid_from <= machine.valid_until) or
                    #  \    |   \+
                    (machine.valid_until is None and valid_until >= machine.valid_from) or
                    #      |    \    |    \ or |   \   \   |
                    (machine.valid_from >= valid_from and machine.valid_from <= valid_until) or
                    # \    |    \    |
                    (machine.valid_until >= valid_from and machine.valid_until <= valid_until) or
                    # \    |         |    \
                    (machine.valid_from <= valid_from and machine.valid_until >= valid_until)
                ):
                    raise ValidationError("Une autre entrée multi-machine est déjà active pour les machines filaires sur ce vlan, à ces dates")


class MachineForm(ModelForm):
    class Meta:
        model = Machine
        exclude = []

    ipv4_address = forms.ModelChoiceField(required=False, queryset=Ipv4Address.objects.none(), help_text="Il faut d'abord créer la machine avant de lui attribuer une ip.", label="adresse ipv4")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(MachineForm, self).__init__(*args, **kwargs)
        try:
            self.fields['ipv4_address'].queryset=Ipv4Address.objects.filter((Q(vlan=self.instance.vlan) | Q(vlan=None)) & (Q(machine=None)|Q(machine=self.instance.pk)))
            self.fields['ipv4_address'].help_text=None
        except Vlan.DoesNotExist:
            pass

    def clean(self, *args, **kwargs):
        if not self.cleaned_data['wifi'] and not self.cleaned_data.get('mac_address', True):
            raise ValidationError("Merci de donner une adresse mac à la machine")
        if self.cleaned_data['ipv4_address'] and self.cleaned_data['ipv4_address'].vlan and self.cleaned_data['ipv4_address'].vlan.id != self.cleaned_data['vlan'].id:
            messages.add_message(self.request, messages.WARNING, "Adresse ip réinitialisé suite à un changement de vlan")
            self.cleaned_data['ipv4_address']=None
        if self.cleaned_data['owner'] and self.cleaned_data['owner'].valid_until != self.cleaned_data['valid_until']:
            if not self.cleaned_data['owner'].valid_until:
                limit = "aucune"
            else:
                limit = self.cleaned_data['owner'].valid_until.strftime("%d/%m/%Y")
            messages.add_message(self.request, messages.WARNING, "Limite de validité fixée à celle du propriétaire : %s" % limit)
            self.cleaned_data['valid_until'] = self.cleaned_data['owner'].valid_until
        if self.cleaned_data['owner'] and self.cleaned_data['owner'].valid_from != self.cleaned_data['valid_from']:
            if not self.cleaned_data['owner'].valid_from:
                limit = "aucune"
            else:
                limit = self.cleaned_data['owner'].valid_from.strftime("%d/%m/%Y")
            messages.add_message(self.request, messages.WARNING, "Limite de début fixée à celle du propriétaire : %s" % limit)
            self.cleaned_data['valid_from'] = self.cleaned_data['owner'].valid_from
        if self.cleaned_data['active'] and not self.cleaned_data['ipv4_address']:
            raise ValidationError("Merci d'attribuer une ipv4 à la machine avant de l'activer")
        return super(MachineForm, self).clean(*args, **kwargs)

    def clean_ipv4_address(self):
        # Sinon je ne sai spas pourquoi, mais le model n'est pas modifié par django
        if not self.cleaned_data['ipv4_address']:
            if self.instance:
                self.instance.ipv4_address = self.cleaned_data['ipv4_address']
        elif not config.allocate_public_ip and not netaddr.IPAddress(self.cleaned_data['ipv4_address']).is_private():
            raise forms.ValidationError("Il n'est pas possible, d'après la configuration, d'allouer des ips publiques aux machines")

        return self.cleaned_data['ipv4_address']

    def clean_mac_address(self):
        if self.cleaned_data['mac_address']:
            try:
                return format_mac(self.cleaned_data['mac_address'])
            except netaddr.AddrFormatError:
                raise forms.ValidationError("Format de l'adresse mac incorrect")
        return self.cleaned_data['mac_address']

    def clean_name(self):
        if len(MultiMachine.objects.filter(name=self.cleaned_data['name'])) > 0:
            raise forms.ValidationError("Nom de machine déjà utilisé comme une multi machine")
        return self.cleaned_data['name']


class OwnerForm(ModelForm):
    class Meta:
        model = Owner
        exclude = []

    def clean(self):
        if self.instance:
            self.check_dates(self.instance, self.cleaned_data['valid_from'], self.cleaned_data['valid_until'])

    @staticmethod
    def check_dates(owner, valid_from, valid_until):
        for machine in owner.multi_machines.all():
            if machine.fixe and machine.active:
                MultiMachineForm.check_dates(machine.vlan, valid_from, valid_until, id=machine.id)
