# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import network_manager.utils
import network_manager.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Interface',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('uplink', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ipv4Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.IPAddressField(unique=True)),
                ('bin', models.BigIntegerField(unique=True, null=True, editable=False, blank=True)),
                ('public', models.NullBooleanField(verbose_name=b'adresse publique', editable=False)),
            ],
            options={
                'ordering': ('bin',),
                'verbose_name': 'adresse ipv4',
                'verbose_name_plural': 'adresses ipv4',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Machine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"Nom d'utilisateur utilis\xc3\xa9 pour la connexion de la machine au r\xc3\xa9seau WiFi.", max_length=255, verbose_name=b'nom')),
                ('wifi', models.BooleanField(default=False, help_text=b'La machine se connecte-t-elle en wifi ou en filaire', verbose_name=b'wifi')),
                ('active', models.BooleanField(default=True, verbose_name=b'active')),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name=b'cr\xc3\xa9ation')),
                ('last_modified', models.DateTimeField(auto_now=True, verbose_name=b'derni\xc3\xa8re modification')),
                ('last_connection', models.DateTimeField(help_text=b'Derni\xc3\xa8re fois que la machine a \xc3\xa9t\xc3\xa9 vue sur le r\xc3\xa9seau.', null=True, verbose_name=b'derni\xc3\xa8re connexion', blank=True)),
                ('valid_until', models.DateField(help_text=b"Date jusqu'\xc3\xa0 laquelle la machine peut s'authentifier. Laissez vide pour pas de limite.", null=True, verbose_name=b'limite de validit\xc3\xa9', blank=True)),
                ('mac_address', models.CharField(help_text=b"L'adresse mac est assign\xc3\xa9e automatiquement \xc3\xa0 la premi\xc3\xa8re connexion en wifi et doit \xc3\xaatre mise \xc3\xa0 la main en filaire", max_length=18, null=True, verbose_name=b'Adresse mac', blank=True)),
                ('wifi_password', models.CharField(default=network_manager.utils.random_wifi_password, help_text=b'G\xc3\xa9n\xc3\xa9r\xc3\xa9 al\xc3\xa9atoirement, ne peut \xc3\xaatre modifi\xc3\xa9.', unique=True, max_length=255, verbose_name=b'mot de passe WiFi')),
                ('manufacturer', models.CharField(help_text=b"Fabricant de la carte r\xc3\xa9seau, si connu (trouv\xc3\xa9 \xc3\xa0 partir de l'adresse MAC).", max_length=255, null=True, verbose_name=b'fabricant', blank=True)),
                ('has_public_ipv4_address', models.NullBooleanField()),
                ('ipv4_address', models.OneToOneField(related_name=b'machine', null=True, blank=True, to='network_manager.Ipv4Address', verbose_name=b'adresse ipv4')),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MachineFirewall',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', models.IntegerField(default=100)),
                ('incoming', models.BooleanField(default=True, verbose_name=b'entrant')),
                ('ip_protocol', models.CharField(default=b'ipv6', max_length=4, verbose_name=b'protocole ip', choices=[(b'ipv4', b'ipv4'), (b'ipv6', b'ipv6'), (b'both', b'both')])),
                ('allow', models.BooleanField(default=True)),
                ('protocol', models.CharField(blank=True, max_length=3, null=True, verbose_name=b'protocole', choices=[(b'udp', b'udp'), (b'tcp', b'tcp')])),
                ('ports', models.CharField(help_text=b'Une liste de port de la forme 22,80,110,25,6000:6010.', max_length=255, null=True, blank=True)),
                ('machine', models.ForeignKey(related_name=b'firewall_rules', verbose_name=b'machine', blank=True, to='network_manager.Machine', null=True)),
            ],
            options={
                'ordering': ('incoming', 'priority', '-machine'),
                'verbose_name': 'r\xe8gle du pare-feu',
                'verbose_name_plural': 'r\xe8gles du pare-feu',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PortRedirection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('public_port', network_manager.fields.IntegerRangeField(null=True, verbose_name=b'port publique', blank=True)),
                ('private_port', network_manager.fields.IntegerRangeField(null=True, verbose_name=b'port priv\xc3\xa9', blank=True)),
                ('protocol', models.CharField(max_length=3, verbose_name=b'protocole', choices=[(b'udp', b'udp'), (b'tcp', b'tcp')])),
                ('private_machine', models.ForeignKey(related_name=b'+', verbose_name=b'machine priv\xc3\xa9e', to='network_manager.Machine')),
                ('public_machine', models.ForeignKey(related_name=b'+', verbose_name=b'machine publique', to='network_manager.Machine')),
            ],
            options={
                'verbose_name': 'redirection de port en ipv4',
                'verbose_name_plural': 'redirections de port en ipv4',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vlan',
            fields=[
                ('id', models.IntegerField(unique=True, serialize=False, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name=b'nom')),
                ('network_ipv4', models.CharField(max_length=255, null=True, blank=True)),
                ('network_ipv6', models.CharField(max_length=255, null=True, blank=True)),
                ('interface', models.OneToOneField(related_name=b'vlan', null=True, blank=True, to='network_manager.Interface')),
            ],
            options={
                'ordering': ('id',),
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='portredirection',
            unique_together=set([('public_machine', 'public_port', 'protocol')]),
        ),
        migrations.AddField(
            model_name='machinefirewall',
            name='vlan',
            field=models.ForeignKey(related_name=b'firewall_rules', blank=True, to='network_manager.Vlan', null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='machinefirewall',
            unique_together=set([('machine', 'incoming', 'protocol', 'allow', 'vlan', 'ip_protocol')]),
        ),
        migrations.AddField(
            model_name='machine',
            name='vlan',
            field=models.ForeignKey(related_name=b'+', to='network_manager.Vlan', help_text=b"En changeant le vlan, l'adresse ip de la machine est r\xc3\xa9initialis\xc3\xa9e"),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='machine',
            unique_together=set([('mac_address', 'vlan'), ('name', 'wifi')]),
        ),
        migrations.AddField(
            model_name='ipv4address',
            name='vlan',
            field=models.ForeignKey(related_name=b'+', blank=True, to='network_manager.Vlan', null=True),
            preserve_default=True,
        ),
    ]
